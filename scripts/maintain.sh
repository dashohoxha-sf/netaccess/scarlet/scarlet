#!/bin/bash
### This script is usually called by update.sh or upgrade.sh,
### or by the button 'Maintain' in the admin interface. It does
### any fixes after the update or the upgrade (e.g. in case that
### the DB structure has been changed, a script should be run
### in order to update it).

### go to the app dir
cd $(dirname $0)
cd ..

### make executable all the script files
find . -name '*.sh' | xargs chmod +x

### compile the translation files
langs="en sq_AL nl it de"
for lng in $langs
do
  l10n/msgfmt.sh $lng
done
