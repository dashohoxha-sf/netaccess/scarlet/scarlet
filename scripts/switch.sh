#!/bin/bash
### switch repository between local and public

if [ "$1" = "" ]
then
  echo "Usage: $0 [local | public]"
  exit 1 
fi
target=$1

local_repo='svn+ssh://dasho@192.168.1.111/data/svn/scarlet/branches/release-3'
public_repo='svn://scarlet.ma-isp.com:88/branches/release-3'

case "$target" in
  local  )  to_repo=$local_repo  ;;
  public )  to_repo=$public_repo ;;
  *      )  echo "Usage: $0 [local | public]" ;  exit 1 ;;
esac

### go to the application directory
cd $(dirname $0)
cd ..

### get the current repository
current_repo=$(svn info | grep '^URL: ' | cut -d ' ' -f2)

### switch the repository
from_repo=$current_repo
svn switch --relocate $from_repo $to_repo

