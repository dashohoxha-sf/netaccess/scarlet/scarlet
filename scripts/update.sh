#!/bin/bash
### This script is usually executed by the button 'Update'
### in the admin interface. It gets any small improvments
### or bug fixes since the last release.

### go to the app dir
cd $(dirname $0)
cd ..

### update any latest changes
svn update

### run the maintenace script to fix anything that needs to be fixed
scripts/maintain.sh

