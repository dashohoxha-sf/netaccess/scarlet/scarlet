#!/bin/bash
### Upgrade automatically to a new release.
###
### Power failure during the upgrade process 
### can create problems, so we should make sure
### that it is done in a transactional way
### (either it is finished completly before the
### power failure, or it resumes automatically 
### from the beginning once the server is rebooted). 

if [ -z "$1" ] 
then
  echo "Usage: $0 nr"
  echo "   where nr is the release number (e.g. 3 or 4)"
  exit
fi

### get the release number
R=$1

### go to the application directory
cd $(dirname $0)
cd ..

function start_upgrade
{
  ### modify /etc/rc.d/rc.local to restart upgrade in case of reboot
  sed -e "/### restart upgrade/,$ d" -i /etc/rc.d/rc.local
  cat <<EOF >> /etc/rc.d/rc.local
### restart upgrade
$(pwd)/make-upgrade.sh
EOF

  ### create 'upgrade-status.txt'
  echo "started" > upgrade-status.txt
}

function make_backup
{
  ### make a backup of the database 
  ### (in case anything goes wrong during upgrade)
  db/backup.sh backup-$(date +%Y%m%d) 

  ### make a backup of the application itself
  ### (in case anything goes wrong during upgrade)
  cd ..
  rm -rf scarlet-backup/
  mkdir scarlet-backup/
  cp -a scarlet/ scarlet-backup/
  cd scarlet/

  ### modify 'upgrade-status.txt'
  echo "backuped" > upgrade-status.txt
}

function make_upgrade
{
  ### get a copy of scarlet from backup 
  ### (in case there was an interruption during upgrade)
  cd ..
  rm -rf scarlet/
  cp -a scarlet-backup/scarlet/ .
  cd scarlet/

  ### upgrade to release-$R
  scripts/upgrade.sh release-$R

  ### modify 'upgrade-status.txt'
  echo "upgraded" > upgrade-status.txt
}

function notify
{
  ### get the ISP details
  . db/vars
  query="SELECT firstname, e_mail, phone1 FROM users WHERE username='ISP'"
  isp_details=$(echo $query | mysql --host=$host --user=$user --password=$passwd --database=$database --table)
  mac=$(scripts/get-mac.sh)

  ### send a notification email
  email='ww@ma-isp.com,dasho@ma-isp.com'
  cat <<EOF | sendmail $email
Subject: Notification: Automatic Upgrade to scarlet-$R

The following router with mac: $mac
has been successfully upgraded automatically to SCARLET release-$R.
ISP Details:
$isp_details

This message is sent automatically by the upgrade script.

EOF

  ### modify 'upgrade-status.txt'
  echo "notified" > upgrade-status.txt
}

function finish
{
  ### remove upgrade script from /etc/rc.d/rc.local
  sed -e "/### restart upgrade/,$ d" -i /etc/rc.d/rc.local

  ### modify 'upgrade-status.txt'
  echo "finished" > upgrade-status.txt
}

if [ ! -f upgrade-status.txt ]
then
  echo "starting" > upgrade-status.txt
fi

while [ -f upgrade-status.txt ]
do
  status=$(cat upgrade-status.txt)
  case $status in
    starting )  start_upgrade ;;
    started  )  make_backup ;;
    backuped )  make_upgrade ;;
    upgraded )  notify ;;
    notified )  finish ;;
    finished )  rm -f upgrade-status.txt ;;
  esac
done
