#!/bin/bash
### get a list of tags

### go to the app dir
cd $(dirname $0)
cd ..

### get the svn url
url=$(svn info | grep 'URL:' | gawk '{print $2}')
url=${url%/branches*}
url=${url%/trunk*}
url="$url/branches/"

### get the list of tags
list=$(svn ls $url)
list=${list//\//}
echo $list

