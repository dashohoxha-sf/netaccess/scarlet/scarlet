#!/bin/bash
### This script is used to upgrade the partition /dev/hda7 to
### fill all the available space in the disk (e.g. in case that a 40GB
### disk of scarlet is copied to new 60GB disk, the script will delete
### /dev/hda7 and will create a new one which fills all the available
### space.
###
### This script needs to reboot the system after the partition table
### has been modified, so that the kernel can load the new partion
### table, and the new partition can be formated, and the rest of the
### configrations can be done. For this reason, it is done in such a way
### that it resumes again after the system is rebooted.

### go to this directory
cd $(dirname $0)

### recreate the new partition /dev/hda7 and reboot 
### so that this partition can be accessed by kernel
function before_reboot
{
  ### modify /etc/rc.d/rc.local to resume upgrade after reboot
  sed -e "/### resume squid upgrade/,+2 d" -i /etc/rc.d/rc.local
  cat <<EndOfPatch >> /etc/rc.d/rc.local
### resume squid upgrade
$(pwd)/squid-upgrade.sh resume

EndOfPatch

  ### shutdown squid and umount the partition (if mounted)
  #/etc/rc.d/rc.squid stop  2> /dev/null
  squid_pid=$(ps ax | grep squid | head -n 1 | gawk '{print $1}')
  kill -9 $squid_pid
  umount /dev/hda7

  ### create a new partition /dev/hda7 in the empty space of the disk
  echo -e "d\n 7\n n\n l\n \n \n p\n w\n" | fdisk /dev/hda

  ### reboot the system
  reboot
}

### format the new partition and use it for the cache of squid
function after_reboot
{
  ### shutdown squid and umount the partition (if mounted)
  #/etc/rc.d/rc.squid stop  2> /dev/null
  squid_pid=$(ps ax | grep squid | head -n 1 | gawk '{print $1}')
  kill -9 $squid_pid
  umount /dev/hda7  2> /dev/null

  ### format /dev/hda7 as ReiserFS
  /sbin/mkreiserfs -q -l squid-cache /dev/hda7 

  ### mount the new partition to /var/log/squid/cache
  mkdir -p /var/log/squid/logs
  mkdir -p /var/log/squid/cache
  mount /dev/hda7 /var/log/squid/cache
  chown nobody:nobody -R /var/log/squid

  ### modify /etc/fstab
  hda6_line="/^\/dev\/hda6/"
  hda7_line="/^\/dev\/hda7/"
  new_line="/dev/hda7  /var/log/squid/cache  reiserfs  defaults  0  0"
  sed -e "$hda7_line d" -i /etc/fstab
  sed -e "$hda6_line a $new_line" -i /etc/fstab

  ### modify the squid config file
  partition_size=$(df | gawk '/hda7/ {print $2}')
  let cache_size=$partition_size/1280  # size / 1024 * 80%
  config_line="cache_dir ufs /var/log/squid/cache $cache_size 16 256"
  squid_conf=../server-config/templates/squid.conf
  sed -e "/^cache_dir/ d" -i $squid_conf
  sed -e "/^# cache_dir/ a $config_line" -i $squid_conf
  ../server-config/update-config-files.sh

  ### recreate the cache directories
  /usr/sbin/squid -z

  ### start squid
  /etc/rc.d/rc.squid start

  ### remove the resume code from /etc/rc.d/rc.local 
  sed -e "/### resume squid upgrade/,+2 d" -i /etc/rc.d/rc.local
}

if [ "$1" != "resume" ]
then before_reboot
else after_reboot
fi
