#!/bin/bash
### get the current tag

### go to the app dir
cd $(dirname $0)
cd ..

### get the svn url
url=$(svn info | grep 'URL:' | gawk '{print $2}')

### get the current tag
tag=${url//*branches\//}
echo $tag

