#!/bin/bash
### return the MAC of eth0

eth0_mac=$(/sbin/ip addr show eth0 | grep 'link/ether' | gawk '{print $2}')
echo $eth0_mac

