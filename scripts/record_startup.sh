#!/bin/bash
### called by /etc/rc.d/rc.local in order to record 
### in the logs the startup of the server

### go to this directory (scripts directory)
cd $(dirname $0)

### get the db parameters
. ../db/vars

### add a record in the table logs of the db
values="time=NOW(), event='startup', details='Source=system'"
echo "INSERT INTO logs SET $values;" \
  | mysql --host=$host --user=$user --password=$passwd --database=$database

