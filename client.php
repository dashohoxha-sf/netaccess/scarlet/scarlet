<?php
/*
Copyright 2001,2002,2003 Dashamir Hoxha, dashohoxha@users.sourceforge.net

This file is part of phpWebApp.

phpWebApp is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

phpWebApp is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with phpWebApp; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  
*/

/** */

define("FIRSTPAGE", "client/login/login.html");

include_once 'webapp.php';

//construct the target page of the transition
$tpl_page = TPL.$event->targetPage;
WebApp::constructHtmlPage($tpl_page);
?>