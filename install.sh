#!/bin/bash
### some commands that are used for the
### installation/configuration of the application

### go to this dir
cd $(dirname $0)

### make executable all the script files
find . -name '*.sh' | xargs chmod +x

### compile wrapper.c and set proper ownership/permissions
gcc -o wrapper wrapper.c
chown root:root wrapper
chmod +s wrapper

### install and init the database
install/db.sh

### compile the translation files
install/l10n.sh

### modify some system configuration files
install/system.sh

### create/modify some system scripts
install/scripts.sh

### modify the php configuration
install/php.sh

### set the superuser password
install/su_passwd.sh

### set the network parameters
install/network.sh

### update some configuration files
server-config/update-config-files.sh
