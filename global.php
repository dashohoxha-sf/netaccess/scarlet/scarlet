<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * Functions and variables that are global
 * and are used through the whole application.
 */

/** Execute a shell command, checking it first for invalid input. */
function shell($cmd)
{
  //some basic check for invalid input
  $cmd = ereg_replace(';.*', '', $cmd);
  $cmd = './wrapper '.$cmd.' 2>&1';
  $output = shell_exec($cmd);
  //print "<xmp>$cmd \n----- \n$output</xmp>\n";  //debug
  return $output;
}

/** Write the content to the file. */
function write_file($fname, &$fcontent)
{
  //get a temp file name
  $tmpfname = tempnam('/tmp', 'NetAccess_'); 

  //write the content to the tmp file
  $fp = fopen($tmpfname, 'w');
  fputs($fp, $fcontent);
  fclose($fp);

  //move to the given fname
  shell("mv -f $tmpfname $fname");

  //fix the file ownership/permissions
  shell("chown root:root $fname");
  shell("chmod +r $fname");
}

/** Add a new log record in the table 'logs'. */
function log_event($event, $details)
{
  $query = "INSERT INTO logs SET time=NOW(),event='$event',details='$details'";
  WebApp::execQuery($query);
}
?>