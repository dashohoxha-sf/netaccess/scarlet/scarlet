
This folder contains some files and folders that are used to generate
automatically some code documentation about netaccess, using some
external tools.

The files and folders that it contains are:
  generate-code-doc.sh -- re-generates the code documentation for netaccess
  doxygen.cfg          -- the configuration for doxygen
  phpdoc_html.sh       -- the configuration for phpDocumentor (HTML output)
  phpdoc_pdf.sh        -- the configuration for phpDocumentor (PDF output)
  netaccess-doxygen/   -- the folder where doxygen outputs the documentation
  netaccess-phpdocu/   -- the folder where phpDocu outputs the documentation

  get/                 -- this folder contains the downloadables of the above 
                          documentatations (.tar.gz)

To generate the doxygen documentation, type: "$ doxygen doxygen.cfg".
'doxygen' usually comes with GNU/Linux distributions, but the latest copy
can be downloaded from www.doxygen.org .

To generate the phpDocumentor documentation, type: "$ ./phpdoc.sh".
It can be downloaded from http://phpdocu.sourceforge.net and it must
be unpacked in the directory netaccess (../..).
