#!/bin/bash
### this script configures the firewall

path=$(dirname $0)
IPT=/usr/sbin/iptables

### clear all the tables
$IPT --table filter --flush
$IPT --table filter --delete-chain
$IPT --table nat    --flush
$IPT --table mangle --flush

### set default policies to ACCEPT
$IPT --table filter --policy INPUT ACCEPT
$IPT --table filter --policy OUTPUT ACCEPT
$IPT --table filter --policy FORWARD ACCEPT
