#!/bin/bash
### Allow the given MAC to connect to internet.

if [ $# = 0 ]
then
  echo "Usage: $0 mac-address"
  exit
fi
MAC=$1

IPT="/usr/sbin/iptables"

### allow connections from this MAC to be forwarded
# delete the last (reject all) rule from the chain
$IPT --table filter --delete FORWARD \
     --jump REJECT --reject-with icmp-net-unreachable

# append another rule into the chain to allow this MAC
$IPT --table filter --append FORWARD \
     --match mac --mac-source $MAC --jump ALLOWED-MACS

# append the last (reject all) rule into the chain
$IPT --table filter --append FORWARD \
     --jump REJECT --reject-with icmp-net-unreachable

### allow the HTTP requests (port 80) from this MAC
# delete the last (redirect) rule from the chain HTTP
$IPT --table nat --delete HTTP --jump REDIRECT

# append another rule into the chain HTTP to allow this MAC
$IPT --table nat --append HTTP \
     --match mac --mac-source $MAC --jump HTTP-ALLOWED-MACS

# append the last (redirect) rule into the chain HTTP
$IPT --table nat --append HTTP --jump REDIRECT

### remove any blocking of established connections for this mac
$IPT --delete FORWARD --match mac --mac-source $MAC \
     --match state --state ESTABLISHED,RELATED --jump REJECT


### create a database for storing the traffic count for this mac
cd $(dirname $0)
../traffic-counter/rrdinit.sh $MAC

