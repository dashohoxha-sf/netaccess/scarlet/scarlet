#!/bin/bash
### the rules of the FORWARD chain

### include network configuration variables
. ../gateway.cfg

IPT='/usr/sbin/iptables --table filter'
APPEND="$IPT --append FORWARD"

### enable forwarding in the kernel
echo 1 > /proc/sys/net/ipv4/ip_forward

### the default is to drop anything that does not match
$IPT --policy FORWARD DROP

### accept any type of icmp 
$APPEND --protocol icmp --icmp-type any --jump ACCEPT

### manage ports (allow or forbid them)
./manage-ports.sh

### accept already established connections
$APPEND --match state --state ESTABLISHED,RELATED --jump ACCEPT

### create the chain ALLOWED-MACS
$IPT --new-chain ALLOWED-MACS
for MAC in $(< allowed-macs)
do
  $APPEND --match mac --mac-source $MAC --jump ALLOWED-MACS
done
### add rules for the chain ALLOWED-MACS
$IPT --append ALLOWED-MACS --jump ACCEPT

### reject anything else
$APPEND --jump REJECT --reject-with icmp-net-unreachable

