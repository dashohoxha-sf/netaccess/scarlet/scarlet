#!/bin/bash
### Manage ports, either allow all except the listed ones
### or forbid all except the listed ones.

IPT='/usr/sbin/iptables --table filter'

### get variables $PORT_LIST, $OPEN_PORTS
. ../gateway.cfg

function allow-ports
{ 
  for PORT in $PORT_LIST
  do 
    $IPT --append CHECK-PORTS --match tcp --protocol tcp \
         --destination-port $PORT --jump RETURN
    $IPT --append CHECK-PORTS --match udp --protocol udp \
         --destination-port $PORT --jump RETURN
  done
  $IPT --append CHECK-PORTS --jump REJECT
}

function forbid-ports
{ 
  for PORT in $PORT_LIST
  do 
    $IPT --append CHECK-PORTS --match tcp --protocol tcp \
         --destination-port $PORT --jump REJECT
    $IPT --append CHECK-PORTS --match udp --protocol udp \
         --destination-port $PORT --jump REJECT
  done
  $IPT --append CHECK-PORTS --jump RETURN
}

### create the chain CHECK-PORTS and jump to it 
### for each packet that is forwarded out through eth0
$IPT --new-chain CHECK-PORTS
$IPT --insert FORWARD --out-interface eth0 \
     --match tcp --protocol tcp --jump CHECK-PORTS
$IPT --insert FORWARD --out-interface eth0 \
     --match udp --protocol udp --jump CHECK-PORTS

### call one of the functions allow-ports or forbid-ports, 
### depending on the value of $OPEN_PORTS
if [ "$OPEN_PORTS" = "true" ] ; then allow-ports ; else forbid-ports ; fi
 
