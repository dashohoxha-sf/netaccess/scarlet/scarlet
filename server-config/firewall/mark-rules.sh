#!/bin/bash
### Mark with 2 the packets that should be routed through the gateway2.

### get variable $G2_PORTS from the configuration file
. ../gateway.cfg

IPT='/usr/sbin/iptables --table mangle'

$IPT --new-chain MARK-RULES
$IPT --append PREROUTING --jump MARK-RULES
$IPT --append OUTPUT     --jump MARK-RULES

for PORT in $G2_PORTS
do
  $IPT --append MARK-RULES --match tcp --protocol tcp \
       --destination-port $PORT --jump MARK --set-mark 0x2
done

