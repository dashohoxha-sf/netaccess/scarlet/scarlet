#!/bin/bash
### redirect some ports (like 53, 80, etc.) to localhost

IPT='/usr/sbin/iptables --table nat'

### get the configuration variables
. ../gateway.cfg

### redirect any DNS requests (port 53) to the localhost
$IPT --append PREROUTING --protocol udp --destination-port 53 --jump REDIRECT
$IPT --append PREROUTING --protocol tcp --destination-port 53 --jump REDIRECT

### handle the HTTP requests (port 80) 
$IPT --new-chain HTTP
$IPT --append PREROUTING --protocol tcp --destination-port 80 --jump HTTP

### handle HTTP requests of the allowed macs in the chain HTTP-ALLOWED-MACS
$IPT --new-chain HTTP-ALLOWED-MACS
for MAC in $(< allowed-macs)
do
  $IPT --append HTTP --match mac --mac-source $MAC --jump HTTP-ALLOWED-MACS
done

### the rules of the chain HTTP-ALLOWED-MACS
if [ "$ENABLE_SQUID" = "false" ]
then
  ### direct connection to internet
  $IPT --append HTTP-ALLOWED-MACS --jump ACCEPT
else
  ### don't use proxy for the connections to the local server
  local_ips="$WAN_IP1 $WAN_IP2 $LAN_IP1 $LAN_IP2 $LAN_IP3"
  for ip in $local_ips
  do
    local_ip=${ip%/*}  ### remove /24 from the ip
    $IPT --append HTTP-ALLOWED-MACS --destination $local_ip --jump ACCEPT
  done
  ### connection through transparent proxy (squid)
  $IPT --append HTTP-ALLOWED-MACS --protocol tcp --jump REDIRECT --to-port 3128
fi

### redirect HTTP requests of the others (not in HTTP-ALLOWED-MACS)
### to the localhost
$IPT --append HTTP --jump REDIRECT

