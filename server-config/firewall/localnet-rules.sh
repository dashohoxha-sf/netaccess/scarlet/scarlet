#!/bin/bash
### The rules of the chain LOCALNET.

IPT='/usr/sbin/iptables --table filter'
APPEND="$IPT --append LOCALNET"

### accept port 67 (dhcp)
./port.sh 67 accept LOCALNET tcp-udp

### accept port 53 (DNS)
./port.sh 53 accept LOCALNET tcp-udp

### accept port 80 (http)
./port.sh 80 accept LOCALNET

### accept port 3128 (proxy/squid)
./port.sh 3128 accept LOCALNET

### accept samba ports
#$IPT --new-chain SAMBA
#$APPEND --jump SAMBA
#./samba-rules.sh

### return to the previous chain
#$APPEND --jump RETURN
#
# This is commented because the default is to return to the previous chain.
# Also, if it is commented, the script 'port.sh' can be used to
# accept or reject any other ports (by appending or deleting rules).
# If it is uncommented, then any other rules that are added later 
# will come after this one, and since this one matches anything, they 
# will not be reached. The order of the rules does matter!

