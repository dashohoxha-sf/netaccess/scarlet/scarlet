#!/bin/bash
### get a list of the macs that are allowed to connect to internet
### and save them in the file 'allowed-macs'

### go to this directory
cd $(dirname $0)

### include the DB connection params
. ../../db/vars

### get them and save them in allowed-macs
mysql --host=$host --user=$user --password=$passwd --database $database \
      --execute="select mac from macs where connected='true'" --vertical \
  | grep '^mac:' | cut -d' ' -f2 \
  > allowed-macs
