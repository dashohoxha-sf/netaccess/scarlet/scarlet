#!/bin/bash
### forward all the packets coming from a given source network
### to another server

function usage
{
  echo "Usage: $0 SOURCE_NET DESTINATION_IP
    SOURCE_NET       the source network (e.g. 192.168.1.0/25)
    DESTINATION_IP   the destination server"
  exit
}

### check that there are at least 2 parameters
if [ $# -lt 2 ]; then usage; fi

SOURCE_NET=$1            # the source network
DESTINATION_IP=${2%/*}   # the destination server

IPT='/usr/sbin/iptables'

### accept for forward everything comming from the $SOURCE_NET
$IPT --table filter --append FORWARD \
     --source $SOURCE_NET --jump ACCEPT
$IPT --table filter --append FORWARD \
     --destination $SOURCE_NET --jump ACCEPT

### forward everything with source $SOURCE_NET to $DESTINATION_IP
$IPT --table nat --append PREROUTING \
     --source $SOURCE_NET --jump DNAT --to-destination $DESTINATION_IP

