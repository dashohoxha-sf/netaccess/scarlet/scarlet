#!/bin/bash
### this script configures the firewall

### go to this directory
cd $(dirname $0)

### include network configuration variables
. ../gateway.cfg

### update the list of allowed macs from DB
./get-allowed-macs.sh

IPT=/usr/sbin/iptables

### clear all the tables
$IPT --table filter --flush
$IPT --table filter --delete-chain
$IPT --table nat    --flush
$IPT --table nat    --delete-chain
$IPT --table mangle --flush
$IPT --table mangle --delete-chain

### accept by default anything in the OUTPUT chain 
$IPT --table filter --policy OUTPUT ACCEPT

### set a mark to each packet that goes to internet
./mark-rules.sh

### forward ports
./forward-ports.sh

### redirect some ports (like 53, 80, etc.) to localhost
./redirect.sh

### enable SNAT-ing
IP1=${WAN_IP1%/*}
IP2=${WAN_IP2%/*}
./source-nat.sh enable eth0 $IP1 $IP2
#./source-nat.sh enable eth0

### the rules of the INPUT chain
./input-rules.sh

### the rules of the FORWARD chain
./forward-rules.sh

### if there is any argument, display the rules
if [ $# != 0 ]
then
  /usr/sbin/iptables-save
fi
