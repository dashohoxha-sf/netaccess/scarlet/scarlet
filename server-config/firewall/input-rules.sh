#!/bin/bash
### the rules of the INPUT chain

### include network configuration variables
. ../gateway.cfg

IPT='/usr/sbin/iptables --table filter'
APPEND="$IPT --append INPUT"

### drop anything that does not match
$IPT --policy INPUT DROP

### accept anything from localhost
$APPEND --in-interface lo --jump ACCEPT

### accept any type of icmp 
$APPEND --protocol icmp --icmp-type any --jump ACCEPT

### accept already established connections
$APPEND --match state --state ESTABLISHED,RELATED --jump ACCEPT

### create the chains WAN and LOCALNET for the connections
### comming from the WAN interface and from the LAN interfaces
$IPT --new-chain WAN
$IPT --new-chain LOCALNET
$APPEND --in-interface eth0 --jump WAN
$APPEND --in-interface ! eth0 --jump LOCALNET

### add the rules for these chains
./wan-rules.sh
./localnet-rules.sh

### reject anything else
$APPEND --jump REJECT --reject-with icmp-host-prohibited

