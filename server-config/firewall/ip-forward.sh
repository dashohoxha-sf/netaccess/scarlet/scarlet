#!/bin/bash
### forward an IP to another server

function usage
{
  echo "Usage: $0 EXTERNAL_IP INTERNAL_IP SERVER_IP
    EXTERNAL_IP  is the IP that will be forwarded
    INTERNAL_IP  is the IP through which it is forwarded
    SERVER_IP    is the IP to which it is forwarded"
  exit
}

### check that there are at least 3 parameters
if [ $# -lt 3 ]; then usage; fi

### get the parameters and remove any netmask from IP-s
EXTERNAL_IP=${1%/*}  # IP that will be forwarded
INTERNAL_IP=${2%/*}  # IP through which it is forwarded
SERVER_IP=${3%/*}    # IP to which it is forwarded

IPT='/usr/sbin/iptables'

### accept for forward everything related with $SERVER_IP
$IPT --table filter --append FORWARD \
     --destination $SERVER_IP --jump ACCEPT
$IPT --table filter --append FORWARD \
     --source $SERVER_IP --jump ACCEPT

### forward everything with destination $EXTERNAL_IP to $SERVER_IP
$IPT --table nat --append PREROUTING \
     --destination $EXTERNAL_IP --jump DNAT --to-destination $SERVER_IP

### in case that $INTERNAL_IP is not gateway for $SERVER_IP
### if this is uncomented, then the internal server does not see
### anything else, except $INTERNAL_IP
#$IPT --table nat --append POSTROUTING \
#     --destination $SERVER_IP --jump SNAT --to-source $INTERNAL_IP

### everything comming from $SERVER_IP should be
### sent out to internet with source $EXTERNAL_IP
#$IPT --table nat --append POSTROUTING \
#     --out-interface eth0 --source $SERVER_IP \
#     --jump SNAT --to-source $EXTERNAL_IP

