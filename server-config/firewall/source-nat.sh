#!/bin/bash
### Enable or disable source NAT (masquerading).
### If two IPs are given, then the second one is used for
### the packets marked with 2, and the first one for the rest.
### If only one IP is given, then the second one is taken to be
### equal to the first one. If no IPs at all are given, then
### MASQUERADING is used instead.

IPT="/usr/sbin/iptables --table nat"

function usage
{
  echo "Usage: $0 [enable|disable|list] [interface] [ip1] [ip2]"
  echo "    ip1 is used for the default traffic"
  echo "    ip2 is used for the packets that have the mark 2"
  echo "    If ip2 is missing, then ip1 is used as ip2."
  echo "    If both ip-s are missing, then masquerading is used instead."
  exit
}

### check that there is one parameter
if [ -z $1 ]; then usage; fi

ACTION=$1
OUT_IF=${2:-eth0}
IP1=$3
IP2=${4:-$IP1}

### enable forwarding in the kernel
echo 1 > /proc/sys/net/ipv4/ip_forward

### append snat rules
function append-snat-rules
{
  delete-snat-rules 2>/dev/null

  $IPT --new-chain SNAT-RULES
  $IPT --append POSTROUTING --out-interface $OUT_IF --jump SNAT-RULES

  if [ -z $IP1 ]
  then
    $IPT --append SNAT-RULES --jump MASQUERADE
  else
    $IPT --append SNAT-RULES \
         --match mark --mark 0x2 --jump SNAT --to-source $IP2    
    $IPT --append SNAT-RULES --jump SNAT --to-source $IP1    
  fi
}

### delete snat rules
function delete-snat-rules
{
  $IPT --delete POSTROUTING --out-interface $OUT_IF --jump SNAT-RULES
  $IPT --flush SNAT-RULES
  $IPT --delete-chain SNAT-RULES
}

### list the existing SNAT rules 
function list-snat-rules
{
  /usr/sbin/iptables-save --table nat | grep 'SNAT-RULES'
}

case $ACTION in
  enable  )  append-snat-rules ;;
  disable )  delete-snat-rules ;;
  list    )  list-snat-rules ;;
  *       )  usage ;;
esac

