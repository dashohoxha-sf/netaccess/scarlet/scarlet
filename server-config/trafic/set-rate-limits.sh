#!/bin/bash
### update the upload and download limits

### go to this dir
cd $(dirname $0)

### get the upload and download rates
. ../gateway.cfg

### set upload limit
if [ "$UPLOAD_LIMIT" = "" ]
then
  ./rate-limit.sh off eth0
else
  ./rate-limit.sh on eth0 $UPLOAD_LIMIT
fi

### set download limit
if [ "$DOWNLOAD_LIMIT" = "" ]
then
  ./rate-limit.sh off eth1
else
  ./rate-limit.sh on eth1 $DOWNLOAD_LIMIT
fi

