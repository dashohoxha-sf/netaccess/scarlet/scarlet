#!/bin/bash
### This script sets a rate limit in one of the network interfaces
### (eth0, eth1, etc.). If the the limit is set in the external interface
### then it is an upload limit; if it is set on the LAN interface, then
### it is a download limit. 

### Set the rate limit to your uplink's *actual* speed, minus a few percent
### If you have a really fast modem, raise 'burst' a bit.

function usage
{
  echo "Usage: $0 [on | off | ls] [dev] [rate]"
  echo "       dev   can be eth0, eth1, etc."
  echo "       rate  can be 48, 96, etc."
  echo
  echo "This script sets a rate limit in one of the network interfaces"
  echo "(eth0, eth1, etc.). If the the limit is set in the external interface"
  echo "then it is an upload limit; if it is set on the LAN interface, then"
  echo "it is a download limit."
  echo
  exit
}

function set_limit
{
  ### check that the rate (third parameter) is supplied
  if [ "$RATE" = "" ]
  then
    echo
    echo "Please supply a rate as well..."
    echo
    usage
    exit 1
  fi

  ### delete any existing settings
  /sbin/tc qdisc del dev $DEV root 2>/dev/null >/dev/null

  ### add the HTB qdisc with just one class (used for limiting the rate)
  /sbin/tc qdisc add dev $DEV root handle 1: htb default 1
  /sbin/tc class add dev $DEV parent 1: classid 1:1 htb rate ${RATE}kbit burst 5k

  ### add a PRIO qdisc below the HTB (in order to 
  ### prioritize interactive traffic)
  /sbin/tc qdisc add dev $DEV parent 1:1 handle 10: prio
  # automatically creates classes 10:1,, 10:2, 10:3

  ### add a SFQ qdisc below each priority class (to handle connections fairly)
  /sbin/tc qdisc add dev $DEV parent 10:1 handle 11: sfq perturb 10
  /sbin/tc qdisc add dev $DEV parent 10:2 handle 12: sfq perturb 10
  /sbin/tc qdisc add dev $DEV parent 10:3 handle 13: sfq perturb 10

  ### Note: with these classes and queues as above, there should be
  ###       no need for filters.

  ### apply the new rate limit
  #/sbin/tc qdisc add dev $DEV root tbf rate ${RATE}kbit latency 50ms burst 3000
}

### get the arguments
if [ $# -lt 2 ]; then usage; fi
ACTION=$1
DEV=$2
RATE=$3

TC="/sbin/tc qdisc"
case $ACTION in
  on  ) set_limit ;;
  off ) /sbin/tc qdisc del dev $DEV root 2>/dev/null >/dev/null;;
  ls  ) /sbin/tc -s qdisc ls dev $DEV ;;
  *   ) usage ;;
esac

