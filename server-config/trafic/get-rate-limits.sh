#!/bin/bash
### get and display the current rate limits

### go to this directory
cd $(dirname $0)

### get the upload rate from eth0 (WAN interface)
upload=$(./rate-limit.sh ls eth0 | cut -d' ' -f5)
rate=$(./rate-limit.sh ls eth0 | cut -d' ' -f4)
if [ "$rate" = "rate" ]
then
  upload=${upload%000bit}
else
  upload=""
fi

### get the download rate from eth1 (LAN interface)
download=$(./rate-limit.sh ls eth1 | cut -d' ' -f5)
rate=$(./rate-limit.sh ls eth1 | cut -d' ' -f4)
if [ "$rate" = "rate" ]
then
  download=${download%000bit}
else
  download=""
fi

### output the results
echo UPLOAD_LIMIT=$upload
echo DOWNLOAD_LIMIT=$download

