#!/bin/bash
### (re)configure the default routes
### if only one parameter is given, then it is used for both gateways
### (gateway1 and gateway2 in this case are the same)

if [ "$1" = "" ]
then
  echo "Usage: $0 gateway1 [gateway2]"
  echo "If only gateway1 is given, then it is used as the second one as well"
  exit 1
fi
 
GATEWAY1=$1
GATEWAY2=${2:-$GATEWAY1}

### change the default route of the main table
/sbin/ip route del default  2> /dev/null
/sbin/ip route add default via $GATEWAY1

### change the default route of the second routing table
/sbin/ip route del table 2 default  2> /dev/null
/sbin/ip route add table 2 default via $GATEWAY2

### delete the cache of routes
/sbin/ip route flush cache
