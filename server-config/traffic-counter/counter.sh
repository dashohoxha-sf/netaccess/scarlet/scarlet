#!/bin/bash
### This script watches the network traffic by reading the output
### of tcpdump through a pipe. It processes this output and accumulates
### the bytes used for upload/download by each MAC.
### It also receives periodically a sigalarm (14) from the script 
### 'timer.sh'. When this signal is received, the number of bytes that
### is counted for each MAC is saved to a file or to a database.

### create upload/download counters (variables) with value 0
### for each database in the directory 'rrd/' 
function init_counters()
{
  db_list=$(ls rrd/)
  for dbname in $db_list
  do
    eval ${dbname}_upload=0
    eval ${dbname}_download=0
  done
}

### save the values of the counters in the databases
function dump_counters()
{
  now=$(date +%s)
  now=$(($now / 10 * 10))
  echo
  for dbname in $db_list
  do
    eval download=\$${dbname}_download
    eval upload=\$${dbname}_upload
    echo update rrd/$dbname $now:$download:$upload
  done

  init_counters
}

### ----------------------------------------------------------

### save the process id to a file
echo $$ > counter.pid

### init counters
init_counters

### call the function dump_counters when the sigalarm is received
trap dump_counters 14

### get the mac of the LAN interface (eth1)
MY_MAC=$(/sbin/ip addr | sed -n -e '/eth1:/,+1 p' | gawk '/link/ {print $2}')

### read package logs from standard input and process them
while read line
do
  ### get from the line $mac1, $mac2 and $bytes
  mac1=${line:18:17}
  mac2=${line:38:17}
  bytes=${line:70:10}
  bytes=${bytes%%:*}

  ### increment the counters
  if [ "$mac1" = "$MY_MAC" ]
  then
    dbname="mac_${mac2//[:-]/}"
    eval let ${dbname}_download+=$bytes
  elif [ "$mac2" = "$MY_MAC" ]
  then
    dbname="mac_${mac1//[:-]/}"
    eval let ${dbname}_upload+=$bytes
  fi

  ### debug
  #echo "$mac1 > $mac2 $bytes"
done

