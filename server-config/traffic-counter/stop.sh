#!/bin/bash

### stop timer
pid=$(cat timer.pid)
kill -9 $pid

### stop tcpdump
pid=$(cat tcpdump.pid)
kill -9 $pid

### clean up any pid files
rm -f *.pid
