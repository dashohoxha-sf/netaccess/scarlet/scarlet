#!/bin/bash

function usage
{
  echo "Usage: $0 fname interval [ mac]+"
  echo "  Generates the traffic usage of one or more macs in an interval."
  echo "  'fname' is the path and name of the image that will be created"
  echo "  'interval' is: 10minutes | 2hours | 2days | 1month | 8months"
  echo "  One or more MACs should be supplied, separated by space."
  exit 1
}

if [[ $# -lt 3 ]] ; then usage ; fi

### go to this directory
cd $(dirname $0)

### get file name and the time interval to be graphed
fname=$1 ; shift
interval=$1 ; shift

### get the resolution
case $interval in
  10minutes )  resolution=10 ;;     # 10 sec
  2hours    )  resolution=300 ;;    # 5 min
  2days     )  resolution=3600 ;;   # 1 hour
  1month    )  resolution=86400 ;;  # 1 day
  8months   )  resolution=86400 ;;  # 1 day
  *         )  usage ;;
esac

### get start and end times
now=$(date +%s)
end=$(($now/$resolution*$resolution))
start="end-$interval"

### process the MACs
nr_macs=0
DEFs=''
upload='0'
download='0'

while [ "$1" != "" ]
do
  let nr_macs++
  mac=$1 ; shift
  mac=${mac//[:-]/}

  DEFs="$DEFs DEF:upload_$mac=rrd/mac_$mac:upload:AVERAGE"
  DEFs="$DEFs DEF:download_$mac=rrd/mac_$mac:download:AVERAGE"

  upload="upload_$mac,$upload,+"
  download="download_$mac,$download,+"
done

### get the average
#upload="$upload,$nr_macs,/"
#download="$download,$nr_macs,/"

### convert from Bytes/s to Bits/s
upload="$upload,8,*"
download="$download,8,*"

### generate the graphic image
rm -f $fname
#echo \
rrdtool graph $fname \
        --imgformat PNG --interlaced --end $end --start $start \
        --width 550 --vertical-label Bits/s    \
        $DEFs                              \
        CDEF:upload=$upload                \
        CDEF:download=$download            \
        AREA:upload#00EEEE:'Upload'        \
        LINE1:upload#00CCAA                \
        LINE1:download#FF0000:'Download'

