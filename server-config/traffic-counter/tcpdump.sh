#!/bin/bash

### run tcpdump on the LAN interface
/usr/sbin/tcpdump -i eth1 -e -n -nn -N -tt -q &

### save the process id to a file
echo $! > tcpdump.pid

###test
#echo $$ > tcpdump.pid
#while true
#do
#  cat test/tcpdump.output
#  sleep 2
#done

