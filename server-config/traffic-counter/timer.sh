#!/bin/bash
### send periodically a sigalarm to the counter

period=10

### save the process id to a file
echo $$ > timer.pid

### get the process id of the counter
while [ "$pid" = "" ]
do
  pid=$(cat counter.pid 2> /dev/null)
done

### send a sigalarm (14) to the counter periodically
while true
do
  sleep $period
  kill -s 14 $pid
done

