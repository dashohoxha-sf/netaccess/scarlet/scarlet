#!/bin/bash

### go to this directory
cd $(dirname $0)

### clean up any pid files
rm -f *.pid

### start tcpdump
#./tcpdump.sh | ./counter.sh &  # debug
./tcpdump.sh | ./counter.sh | rrdtool - >/dev/null &

### start timer
./timer.sh &

