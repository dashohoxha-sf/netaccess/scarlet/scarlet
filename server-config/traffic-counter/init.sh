#!/bin/bash
### initialize the databases for each allowed mac

rm -rf rrd/
mkdir -p rrd
for mac in $(< ../firewall/allowed-macs)
do
  ./rrdinit.sh $mac
done
