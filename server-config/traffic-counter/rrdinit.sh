#!/bin/bash
### Create a RRD for the MAC that is given as argument.
### This database keeps a DS for download and one for upload.
### The interval in which the data is fed into the database is 10 sec.
### and the type of the data is ABSOLUTE (after feeding the data into
### the database the counters that count upload and download traffic
### are reseted to 0).
###
### The round robin archives that are kept in the database are these:
###
### - One that keeps each data that is inputed and has 60 items
###   (time interval 10s, time span 10m.)
###
### - One that consolidates 30 primary values for each item and has 24 items
###   (time interval 300s = 5m, time span 120m = 2h)
###
### - One that consolidates 360 values for each item and has 48 items
###   (time interval 3600s = 1h, time span 48h = 2 days)
###
### - One that averages 360*24=8640 values and has 240 items
###   (time interval 1h*24 = 1day, time span 1day * 240 = 8 months)

if [ "$1" = "" ]
then
  echo "Usage: $0 mac"
  exit 1
fi

mac=$1
dbname="mac_${mac//[:-]/}"

### go to this directory
cd $(dirname $0)

### don't overwrite an existing database
if [ -f "rrd/$dbname" ] ; then exit ; fi

### calculate start time to be a multiple of 86400 (one day)
now=$(date +%s)
day=86400
start=$(( $now / $day * $day ))

### create a new database
rrdtool create rrd/$dbname \
        --start $start --step 10 \
        DS:download:ABSOLUTE:30:0:U \
        DS:upload:ABSOLUTE:30:0:U \
        RRA:AVERAGE:0.5:1:60 \
        RRA:AVERAGE:0.5:30:24 \
        RRA:AVERAGE:0.5:360:48 \
        RRA:AVERAGE:0.5:8640:240

