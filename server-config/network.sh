#!/bin/bash
### reconfigure the network after changing any parameters

### go to this dir
cd $(dirname $0)

### variables
IP=/sbin/ip

### include the configuration file
. ./gateway.cfg

### set up the links, in  case that they are not up
$IP link set eth0 up
$IP link set eth1 up

### flush any existing ip addresses
$IP address flush eth0
$IP address flush eth1

### flush any existing routes of table main
$IP route flush table main

### add new addresses
$IP address add $WAN_IP1 dev eth0
$IP address add $WAN_IP2 dev eth0
$IP address add $LAN_IP1 dev eth1

### copy the routes of the main table to another routing table
$IP route flush table 2
$IP route show table main | grep -Ev ^default \
  | while read ROUTE ; do $IP route add table 2 $ROUTE ; done

### add the default routes
./set-default-routes.sh $GATEWAY1 $GATEWAY2

### use the second routing table for the packets marked with 2 by the firewall
$IP rule del fwmark 2 table 2   2>/dev/null
$IP rule add fwmark 2 table 2

### add the access IP to the external (WAN, eth0) interface
$IP address add $ACCESS_IP dev eth0

$IP route flush cache
