#!/bin/bash
### modify the configuration files after changing
### any configuration variables

### go to this directory
cd $(dirname $0)

### include the configuration file
. gateway.cfg

### change the resolv.conf files
echo "nameserver $DNS1" > /etc/resolv.conf
echo "nameserver $DNS1" > /etc/resolv.dnsmasq.conf
if [ -n "$DNS2" ] 
then
  echo "nameserver $DNS2" >> /etc/resolv.conf
  echo "nameserver $DNS2" >> /etc/resolv.dnsmasq.conf
fi
if [ -n "$DNS3" ] 
then
  echo "nameserver $DNS3" >> /etc/resolv.conf
  echo "nameserver $DNS3" >> /etc/resolv.dnsmasq.conf
fi

### update /etc/hosts
LOCAL_IP=${LAN_IP1%/*}
sed -e "s/|DOMAIN|/$DOMAIN/g" \
    -e "s/|LOCAL_IP|/$LOCAL_IP/g" \
    templates/hosts > /etc/hosts

### change the configuration of dnsmasq
LAN_NETWORK=${LAN_IP1%.*/*}
sed -e "s/|DOMAIN|/$DOMAIN/g"  \
    -e "s/|LAN_NETWORK|/$LAN_NETWORK/g"        \
    templates/dnsmasq.conf > /etc/dnsmasq.conf
if [ "$ENABLE_DHCPD" = "false" ]
then
  ### comment any DHCP settings
  sed -e "s+^dhcp+#dhcp+" -i /etc/dnsmasq.conf
fi

### change the configuration of HTTPD
APP_PATH=$(dirname $(pwd))
sed -e "s#|APP_PATH|#$APP_PATH#g"  \
    templates/httpd.conf > /etc/apache/httpd.conf

### change the configuration of proxy (squid)
LAN1=${LAN_IP1%.*/*}.0/24
OUR_NETWORKS="$LAN1"
sed -e "s+|OUR_NETWORKS|+$OUR_NETWORKS+g"  \
    templates/squid.conf > /etc/squid/squid.conf

