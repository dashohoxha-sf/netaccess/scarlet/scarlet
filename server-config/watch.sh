#!/bin/bash
### Check periodicly the status of the gateways (whether they
### are dead or alive) and modify the network configuration
### accordingly, so that if one line goes down it is backup-ed
### by the other one.

### go to this directory
cd $(dirname $0)

### read the config file
. gateway.cfg

### ping each gateway and find out whether it is on or off
function check-status
{
  if  ping -r -I eth0 -c 2 -W 1 $GATEWAY1 > /dev/null 2> /dev/null
  then G1=on ; else G1=off
  fi

  if  ping -r -I eth0 -c 2 -W 1 $GATEWAY2 > /dev/null 2> /dev/null
  then G2=on ; else G2=off
  fi
}

### compare the status with the status of the previous check,
### return true if the status of the gateways has changed
function status-changed
{
  test "$G1" != "$G_1" -o "$G2" != "$G_2" 
}

### change the default routes and the SNAT rules of the firewall
function change-network-config
{
  #echo $G1 -- $G2  # debug

  IP1=${WAN_IP1%/*}
  IP2=${WAN_IP2%/*}

  if [ "$G1" = "on" ]
  then 
    gateway1=$GATEWAY1
    ip1=$IP1
  else
    gateway1=''
    ip1=''
  fi

  if [ "$G2" = "on" ]
  then
    gateway2=$GATEWAY2
    ip2=$IP2
  else
    gateway2=''
    ip2=''
  fi

  ### if at least one of the gateways is on
  ### then change the config of the default routes and SNAT
  if [ "$G1" = "on"  -o  "$G2" = "on" ]
  then 
    ./set-default-routes.sh $gateway1 $gateway2
    firewall/source-nat.sh enable eth0 $ip1 $ip2
  fi
}

### save the current status of the gateways
### in order to compare them in the next check
function save-current-status
{
  G_1=$G1
  G_2=$G2
}

### start an endless loop that periodicly checks the status of the interfaces,
### and if it is changed, updates the network config accordingly
while true
do
  check-status

  if status-changed
  then
    change-network-config
    save-current-status
  fi

  sleep 6   # wait for a while
done
