<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package connect
 */
class connect extends WebObject
{
  function init()
    {
      $this->addSVar('page', 'login');  // login | info | connected
      $this->addSVar('username', UNDEFINED);
    }
  
  function on_set_page($event_args)
    {
      $this->setSVar('page', $event_args['page']);
    }

  /** return the MAC of the client */
  function get_mac()
    {
      $ip = $_SERVER['REMOTE_ADDR'];
      shell("ping -c 3 $ip");
      $output = shell("/sbin/ip neigh show to $ip | cut -d ' ' -f 5");
      $arr_lines = explode("\n", $output);
      $mac = $arr_lines[0];
      return $mac;
    }

  function on_login($event_args)
    {
      //authenticate the user (check the username and password)
      $rs = WebApp::openRS('authenticate', $event_args);
      if ($rs->EOF())  //no client with such a code
        {
          $msg = T_("There is no user with such a username and password! \
Please try again.");
          WebApp::message($msg);
          return;
        }

      //keep the username of the client
      $this->setSVar('username', $event_args['username']);

      //get the client limits
      $rs = WebApp::openRS('get_client_limits', $event_args);
      $nr_connections = $rs->Field('nr_connections');
      $expiration_time = $rs->Field('expiration_time');
      $upload_limit = $rs->Field('upload_limit');
      $download_limit = $rs->Field('download_limit');

      //check that the expiration time has not passed
      if (strtotime($expiration_time) < time())
        {
          $msg = T_("The time limit of this internet user has expired.\n\
Please contact your Internet Service Provider.");
          WebApp::message($msg);
          return;
        }

      //check that the client has available traffic to consume
      if (($upload_limit <= 0) or ($download_limit <=0))
        {
          $msg = T_("There is no traffic to be used for this internet user.\n\
Please contact your Internet Service Provider.");
          WebApp::message($msg);
          return;
        }

      //check that the max nr of connections is not passed
      $rs = WebApp::openRS('connected_macs', $event_args);
      $arr_macs = $rs->getColumn('mac');
      $mac = $this->get_mac();
      if (!in_array($mac, $arr_macs) and $rs->count >= $nr_connections)
        {
          $msg = T_("This user can have up to var_nr_conn computers \n\
connected to internet and this number has been reached. \n\
If you want to replace one of the computers with another, \n\
please contact your Internet Service Provider.");
          $msg = str_replace('var_nr_conn', $nr_connections, $msg);
          WebApp::message($msg);
          return;          
        }

      //check that this mac is not registered to another client
      $rs = WebApp::openRS('get-mac', compact('mac'));
      if (!$rs->EOF() and $rs->Field('client')!=$event_args['username'])
        {
          $msg = T_("This computer is registered for another client.");
          WebApp::message($msg);
          return;          
        }

      WebApp::addSVar('username', $event_args['username'], 'DB');
      $this->setSVar('page', 'connected');
    }

  function onParse()
    {
      $page = $this->getSVar('page');
      switch ($page)
        {
        default:
        case 'login':
          $title = T_("Connect my computer ...");
          $file = 'login/login.html';
          break;
        case 'info':
          $title = T_("Information ...");
          $file = 'info/info.html';
          break;
        case 'connected':
          $title = T_("You are connected ...");
          $file = 'connected/connected.html';
          break;
        }
      WebApp::addGlobalVar('page_title', $title);
      WebApp::addVar('file', $file);
    }

  function onRender()
    {
      $rs = WebApp::openRS('get_contacts');
      if (!$rs->EOF())
        {
          WebApp::addVars($rs->Fields());
        }
      else
        {
          $arr_contacts = array(
                                'isp_name' => 'Fastlink Internet Café',
                                'address'  => 'Rr. Islam Alla 34, Tirane',
                                'phone'    => '04-253867',
                                'e_mail'   => 'info@fastlink-internet.com'
                                );
          WebApp::addVars($arr_contacts);
        }
    }
}
?>