// -*-C-*-
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


function send_event(obj_id, event_name, event_args)
{
  if (event_args==null)  event_args = "";
  else  event_args = "(" + event_args + ")";

  var target = 'http://gateway.localnet.net/connect.php';
  GoTo("thisPage?event=" + obj_id + "." + event_name + event_args, target);
}

function view_page(page)
{
  send_event('connect', 'set_page', 'page='+page); 
}

function login()
{
  var form = document.login_form;
  var username = form.username.value;
  var password = form.password.value;

  if (username=='')
    {
      alert(T_("Please enter the name!"));
      form.username.focus();
      return;
    }

  if (password=='')
    {
      alert(T_("Please enter the code!"));
      form.password.focus();
      return;
    }

  var event_args = 'username=' + username + ';password=' + password;
  send_event('connect', 'login', event_args);
}

function open_page()
{
  window.location.href = 'http://www.google.com/';
}

function client_stats()
{
  var page = 'client/account/client.html';
  var target = 'http://gateway.localnet.net/client.php';
  var username = session.getVar('connect->username');
  var event_args = '(username='+username+')';

  GoTo(page+'?event=client.login'+event_args, target);
}

//------------- image functions ---------------

function preload_images()
{
  var name, src, fname, fname_mover, src_mover;
  var img_obj = new Image();
  var i = 0;
  while (document.images[i])
    {
      image = document.images[i];

      name = image.name;
      src = image.src;
      fname = name + '\.png';
      fname_mover = name + '-over.png';
      src_mover = src.replace(fname, fname_mover);

      img_obj.src = src_mover;

      i++;
    }
}

function mouseover(image)
{
  var name = image.name;
  var src = image.src;
  var fname = name + '\.png';
  var new_fname = name + '-over.png';
  var new_src = src.replace(fname, new_fname);

  image.src = new_src;
}

function mouseout(image)
{
  var name = image.name;
  var src = image.src;
  var fname = name + '-over\.png';
  var new_fname = name + '.png';
  var new_src = src.replace(fname, new_fname);

  image.src = new_src;
}
