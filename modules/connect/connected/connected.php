<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


/**
 * @package connect
 */
class connected extends WebObject
{
  /** return the MAC of the client that has the given IP */
  function get_mac($ip)
    {
      shell("ping -c 3 $ip");
      $output = shell("/sbin/ip neigh show to $ip | cut -d ' ' -f 5");
      $arr_lines = explode("\n", $output);
      $mac = $arr_lines[0];
      return $mac;
    }

  /** register the MAC for this user in the table 'macs' */
  function register_mac($client, $mac, $hostname)
    {
      //if this MAC is already there and it is connected, do nothing
      $rs = WebApp::openRS('get-mac', compact('mac'));
      if (!$rs->EOF() and $rs->Field('connected')=='true')  return; 

      //add it in the table or update it
      $timestamp = time();
      $params = compact('client', 'mac', 'timestamp', 'hostname');
      if ($rs->EOF())
        WebApp::execDBCmd('add-mac', $params);
      else
        WebApp::execDBCmd('update-mac', $params);

      //allow this MAC in the firewall
      $path = APP_PATH."server-config/firewall";
      shell("$path/mac-allow.sh $mac");

      //log the event
      $d = "Source=client, Client=$client, MAC=$mac, Comment: registration";
      log_event('+MAC', $d);

      //reinit the clients of the script 'check-traffic-limits.php'
      shell('touch cron/reinit-clients.flag');
    }

  /**
   * Get the name of the client computer, using the command smbclient.
   */
  function get_comp_name($ip)
    {
      $output = shell("smbclient --list $ip --no-pass --grepable");
      $arr_lines = explode("\n", $output);
      for ($i=0; $i < sizeof($arr_lines); $i++)
        {
          $line = $arr_lines[$i];
          if (!preg_match('#Domain=#', $line)) continue;
          else
            {
              preg_match('#Domain=\[(.+?)\]#', $line, $matches);
              $comp_name = $matches[1];
              return $comp_name;
            }
        }
      return '';
    }

  function onRender()
    {
      $username = WebApp::getSVar('username');
      $ip = $_SERVER['REMOTE_ADDR'];
      $mac = $this->get_mac($ip);
      $computer = $this->get_comp_name($ip);
      $this->register_mac($username, $mac, $computer);

      //get client details
      $rs = WebApp::openRS('get-client-details', compact('username'));
      $client_record = $rs->Fields();
      $firstname = $client_record['firstname'];
      $lastname = $client_record['lastname'];
      $clientname = "$firstname $lastname";

      //bandwidth
      $arr_settings = $this->get_settings();
      $download = $arr_settings['DOWNLOAD_LIMIT'];
      $upload = $arr_settings['UPLOAD_LIMIT'];
      //$bandwidth = "$download -- $upload";
      $bandwidth = "512 -- 128";

      $expiration = $client_record['expiration_time'];
      $expiration = date('Y-m-d H:i', strtotime($expiration));
      $nr_connections = $client_record['nr_connections'];

      //count free connections
      $rs = WebApp::openRS('get-client-used-cnns', compact('username'));
      $used_connections = $rs->Field('used_connections');
      $free_connections = $nr_connections - $used_connections;

      //add template vars
      WebApp::addVars(compact('ip', 'mac', 'username', 'computer',
                              'clientname', 'bandwidth', 'expiration',
                              'nr_connections', 'free_connections'));
    }

  /** returns an associative array with the settings and their values */
  function get_settings()
    {
      $arr_settings = array();
      $rs = WebApp::execQuery('SELECT * FROM settings');
      while (!$rs->EOF())
        {
          $name = $rs->Field('name');
          $value = $rs->Field('value');
          $arr_settings[$name] = $value;
          $rs->MoveNext();
        }

      return $arr_settings;
    }
}
?>