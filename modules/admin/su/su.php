<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class su extends WebObject
{
  function on_register_router($event_args)
    {
      //get eth0 MAC
      $eth0_mac = shell('scripts/get-mac.sh');
      $eth0_mac = trim($eth0_mac);

      //get the ISP name
      $query = "SELECT firstname FROM users WHERE username = 'ISP'";
      $rs = WebApp::execQuery($query);
      $isp = $rs->Field('firstname');

      //get the registration data
      $arr_data = $event_args;  //username and password
      $arr_data['mac'] = $eth0_mac;
      $arr_data['isp'] = $isp;
      //add some language vars (to get the right translation for the messages)
      $arr_data['lng'] = WebApp::getSVar('lng');
      $arr_data['codeset'] = WebApp::getSVar('codeset');

      //send the registration info to the router admin and get the reply
      $data = serialize($arr_data);
      $data = rawurlencode($data);
      $url = ROUTER_ADMIN_URL.'register-router.php';
      $reply = file("$url?$data");
      //WebApp::message("Reply:\n\n" . implode("\n", $reply));  //debug

      $status = trim($reply[0]); 
      $message = trim($reply[1]); 
      if ($status=='OK')
        {
          //save the password in a local file
          $router_passwd = shell("openssl passwd -1 '$eth0_mac'");
          write_file('.su/routerpasswd', $router_passwd);

          //run maintain.php weekly but on a random time
          shell('install/scripts.sh');

          //display the confirmation message
          WebApp::message($message);
        }
      elseif ($status=='NOK')
        {
          //display the notification message
          WebApp::message($message);          
        }
      else
        {
          //give a failure message/notification
          $msg = T_("Registration failed. Maybe there is no net connection?");
          WebApp::message($msg);
        }
    }

  function on_init_db($event_args)
    {
      $output = shell("db/init.sh 2>&1");
      if (trim($output)!='')
        WebApp::debug_msg("<xmp>$output</xmp>");      
    }

  function on_maintain($event_args)
    {
      $script = $event_args['script'];
      //$server = '213.247.46.14';
      $server = 'scarlet.ma-isp.com';
      $url = "http://$server/rscripts/$script";
      shell(".su/run-script.sh $url $script");
    }
}
?>