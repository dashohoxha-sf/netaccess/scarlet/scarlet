<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


/**
 * @package    admin
 * @subpackage stats
 */

class traffic_stats extends WebObject
{
  function init()
    {
      $this->addSVar('rs_id', 'paged_client_list');
      $this->addSVar('client_filter', '(1=1)');
    }

  function on_next($event_args)
    {
      $page = $event_args['page'];
      $rs_id = $this->getSVar('rs_id');
      WebApp::setSVar("${rs_id}->current_page", $page);
    }

  function onRender()
    {
      $this->set_rs_id();
      $this->set_columns_rs();
      $this->build_stats_rs();
    }

  function set_rs_id()
    {
      $print = WebApp::getVar('PRINT');
      $macs = WebApp::getSVar('statsFilter->macs');
      $rs_type = ($print=='true' ? 'full' : 'paged');
      $rs_detail = ($macs=='true' ? 'mac' : 'client');

      $rs_id = "${rs_type}_${rs_detail}_list";
      $this->setSVar('rs_id', $rs_id);

      if ($rs_type=='$paged')  WebApp::setSVar("${rs_id}->recount", 'true');
    }

  /** creates a recordset with the time columns of the report */
  function set_columns_rs()
    {
      $date1 = WebApp::getSVar('statsFilter->date1');
      $date2 = WebApp::getSVar('statsFilter->date2');
      $time1 = strtotime($date1);
      $time2 = strtotime($date2);

      $step = WebApp::getSVar('statsFilter->step');
      switch ($step)
        {
        default:
        case 'day':
          $rs = $this->get_columns_day($time1, $time2);
          break;
        case 'week':
          $rs = $this->get_columns_week($time1, $time2);
          break;
        case 'month':
          $rs = $this->get_columns_month($time1, $time2);
          break;
        }

      $rs->ID = 'columns';
      global $webPage;
      $webPage->addRecordset($rs);
    }

  function get_columns_day($t1, $t2)
    {
      $columns = new EditableRS('columns');

      $day = 24*60*60;

      for ($t=$t1; $t <= $t2; $t+=$day)
        {
          $id = date('dMY', $t);
          $title = date('d M Y', $t);
          $time1 = $t;
          $time2 = $t + $day;
          $columns->addRec(compact('id', 'title', 'time1', 'time2'));
        }

      //print $columns->toHtmlTable();  //debug
      return $columns;
    }

  function get_columns_week($t1, $t2)
    {
      $columns = new EditableRS('columns');

      $day = 24*60*60;
      $week = 7*$day;

      $d1 = date('w', $t1);       //day of week (0-sunday, 6-saturday)
      if ($d1==0)  $d1 = 6;  else  $d1 -= 1;  //(0-monday, 6-sunday)
      $d2 = date('w', $t2);       //day of week (0-sunday, 6-saturday)
      if ($d2==0)  $d2 = 6;  else  $d2 -= 1;  //(0-monday, 6-sunday)

      for ($t=$t1-$d1*$day; $t < ($t2+(7-$d2)*$day); $t+=$week)
        {
          $time1 = $t;
          $time2 = $t + 7*$day;

          $t_1 = ($time1 < $t1 ? $t1 : $time1);
          $t_2 = ($time2 > $t2 ? $t2 : $time2-$day);
          $id = date('dMY', $t_1).date('dMY', $t_2);
          $title = date('d', $t_1).' - '.date('d M', $t_2);

          $columns->addRec(compact('id', 'title', 'time1', 'time2'));
        }

      //print $columns->toHtmlTable();  //debug
      return $columns;
    }

  function get_columns_month($t1, $t2)
    {
      $columns = new EditableRS('columns');

      $m1 = date('n', $t1);
      $y1 = date('Y', $t1);
      $t = mktime(0, 0, 0, $m1, 1, $y1);
      while ($t < $t2)
        {
          $time1 = $t;
          $m1++;
          $t = mktime(0, 0, 0, $m1, 1, $y1);
          $time2 = $t;

          $id = date('MY', $time1);
          $title = date('M Y', $time1);
          if ($time1 < $t1 or $time2 > $t2 + 24*60*60)
            {
              $d1 = ($time1 < $t1 ? date('d', $t1) : date('d', $time1));
              $d2 = ($time2 > $t2 ? date('d', $t2) : date('d', $time2-1));
              $title = "$d1-$d2 " . $title;
            }

          $columns->addRec(compact('id', 'title', 'time1', 'time2'));
        }

      //print $columns->toHtmlTable();  //debug
      return $columns;
    }

  function build_stats_rs()
    {
      //get the stats recordset (that will be displayed in the report)
      $rs_id = $this->getSVar('rs_id');
      $stats = WebApp::openRS($rs_id);
      $this->add_period_columns($stats);

      //calculate the upload/download values for each record of $stats 
      //(for each row of the report)
      $mac_details = WebApp::getSVar('statsFilter->macs');
      $stats->MoveFirst();
      while (!$stats->EOF())
        {
          //get a row_filter, which will be used to get all the logs
          //that are related to the current row (record of $stats),
          //which can be a client or a mac
          $client = $stats->Field('client');
          $row_filter = "client='$client'";
          if ($mac_details=='true')
            {
              $mac = $stats->Field('mac');
              $row_filter .= " AND mac='$mac'";
            }

          //get the logs that will be used to calculate
          //the data of the current row of stats 
          $row_logs = WebApp::openRS('get_row_logs', compact('row_filter'));
          //WebApp::debug_msg($row_logs->toHtmlTable());  //debug

          //calculate the data of the current row of stats
          $this->calculate_stats_row($stats, $row_logs);

          $stats->MoveNext();
        }

      $this->add_horizontal_totals($stats);
      $this->add_vertical_totals($stats);

      //add the recordset to $webPage
      global $webPage;
      $webPage->addRecordset($stats);
    }

  /** Add new columns to the $stats recordset for each period of time. */
  function add_period_columns(&$stats)
    {
      $cols = WebApp::openRS('columns');
      $cols->MoveFirst();
      while (!$cols->EOF())
        {
          $col_id = $cols->Field('id');
          $stats->addCol($col_id, 0.0);
          $cols->MoveNext();
        }

      //add a total column
      $stats->addCol('total', 0.0);
    }

  /**
   * Sum up the logs for the current row/record (which can be a client
   * or a mac), for each display period (which can be days, weeks or months).
   */
  function calculate_stats_row(&$stats, &$logs)
    {
      $cols = WebApp::openRS('columns');

      $logs->MoveFirst();
      while (!$logs->EOF())
        {
          $cols->MoveFirst();
          while (!$cols->EOF())
            {
              $upload = 0.0;
              $download = 0.0;
              $time2 = $cols->Field('time2');
              while (!$logs->EOF() and 
                     strtotime($logs->Field('date')) < $time2)
                {
                  $upload += $logs->Field('upload');
                  $download += $logs->Field('download');
                  $logs->MoveNext();
                }

              //save the calculated upload/download in a field of $stats
              $col_id = $cols->Field('id');
              $upload = number_format($upload, 1);
              $download = number_format($download, 1);
              $stats->setFld($col_id, "$upload / $download");

              $cols->MoveNext();
            }
        }
    }

  function add_horizontal_totals(&$stats)
    {
      $cols = WebApp::openRS('columns');
      $stats->MoveFirst();
      while (!$stats->EOF())
        {
          $upload = 0.0;
          $downlod = 0.0;
          $cols->MoveFirst();
          while (!$cols->EOF())
            {
              $col_id = $cols->Field('id');
              list($upld,$downld) = explode('/', $stats->Field($col_id));
              $upld = str_replace(',', '', trim($upld));
              $downld = str_replace(',', '', trim($downld));
              $upload += $upld;
              $download += $downld;
              $cols->MoveNext();
            }
          $upload = number_format($upload, 1);
          $download = number_format($download, 1);
          $stats->setFld('total', "$upload / $download");
          $stats->MoveNext();
        }
    }

  function add_vertical_totals(&$stats)
    {
      $total_upload = 0.0;
      $total_download = 0.0;

      $cols = WebApp::openRS('columns');
      while (!$cols->EOF())
        {
          $col_id = $cols->Field('id');
          $arr = $stats->getColumn($col_id);
          $upload = 0.0;
          $download = 0.0;
          for ($i=0; $i < sizeof($arr); $i++)
            {
              list($upld,$downld) = explode('/', $arr[$i]);
              $upld = str_replace(',', '', trim($upld));
              $downld = str_replace(',', '', trim($downld));
              $upload += $upld;
              $download += $downld;
            }

          $total_upload += $upload;
          $total_download += $download;

          $upload = number_format($upload, 1);
          $download = number_format($download, 1);
          WebApp::addVar("total_${col_id}", "$upload / $download");

          $cols->MoveNext();
        }
      $total_upload = number_format($total_upload, 1);
      $total_download = number_format($total_download, 1);
      WebApp::addVar('total_totals', "$total_upload / $total_download");
    }
}
?>