<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    admin
 * @subpackage stats
 */
class statsFilter extends WebObject
{
  function init()
    {
      $this->addSVars(array(
                            'client' => '',
                            'date1'  => date('Y-m-d', time() - 5*24*60*60),
                            'date2'  => date('Y-m-d', time()),
                            'macs'   => 'false',
                            'step'   => 'day'  //(day|week|month)
                            ));
    }

  function on_refresh($event_args)
    {
      //do nothing
    }

  function onParse()
    {
      WebApp::setSVar('traffic_stats->filter', $this->get_filter());
    }

  function get_filter()
    {
      //get the state vars as local variables
      $vars = $this->getSVars();
      array_walk($vars, 'trim');
      extract($vars);

      $conditions = array();

      //get the filter conditions
      $conditions[] = "(date >= '$date1' AND date <= '$date2')";
      if ($client!='')  $conditions[] = "client LIKE '%$client%'";

      $filter = '(' . implode(' AND ', $conditions) . ')';
      return $filter;
    }

  function onRender()
    {
      $rs = new EditableRS('rs_steps');
      $rs->addRec(array('id'=>'day', 'label'=>T_("day")));
      $rs->addRec(array('id'=>'week', 'label'=>T_("week")));
      $rs->addRec(array('id'=>'month', 'label'=>T_("month")));

      global $webPage;
      $webPage->addRecordset($rs);
    }
}
?>