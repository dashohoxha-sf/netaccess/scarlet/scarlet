// -*-C-*- //tell emacs to use C mode
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function refresh()
{
  SendEvent('misc', 'refresh');
}

function reboot()
{
  var msg = T_("You are rebooting the router!");
  if (!confirm(msg))  return;

  SendEvent('misc', 'reboot');
}

function update_software()
{
  var msg = T_("Are you sure that you want to update the software?");
  if (!confirm(msg))  return;

  SendEvent('misc', 'update_software');
}

function upgrade_software()
{
  var msg = T_("Upgrading the software sometimes may cause any problems,\n\
so, make sure that in any case you have a backup of the data.\n\
\n\
Are you sure that you want to upgrade the software?");
  if (!confirm(msg))  return;
 
  var tag = prompt(T_("Upgrading to: "));
  if (tag==null)  return;

  SendEvent('misc', 'upgrade_software', 'tag='+tag);
}

function set_time()
{
  var form = document.set_time;
  var time = form.time.value;
  SendEvent('misc', 'set_time', 'time='+time);
}
