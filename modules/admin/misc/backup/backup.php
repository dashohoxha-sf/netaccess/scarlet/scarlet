<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class backup extends WebObject
{
  /** refresh the list after uploading a file */
  function on_refresh($event_args)
    {
      sleep(1);
    }

  /** delete a backup file */
  function on_delete($event_args)
    {
      $fname = $event_args['fname'];
      shell("rm db/backup/$fname");
      $msg = T_("File 'v_fname' deleted.");
      $msg = str_replace('v_fname', $fname, $msg);
      WebApp::message($msg);
    }

  function on_make_backup($event_args)
    {
      $date = date('Ymd');
      $output = shell("db/backup.sh backup-$date 2>&1");
      if (trim($output)!='')
        WebApp::debug_msg("<xmp>$output</xmp>");      
    }

  function on_restore($event_args)
    {
      $fname = $event_args['fname'];
      $output = shell("db/restore.sh $fname");
      if (trim($output)!='')
        {
          $msg = "Restoring file 'v_fname' failed.";
          $msg = str_replace('v_fname', $fname, $msg);
          WebApp::message($msg);
          WebApp::debug_msg("<xmp>$output</xmp>");
        }
      else
        {
          $msg = "Backup file 'v_fname' restored successfully.";
          $msg = str_replace('v_fname', $fname, $msg);
          WebApp::message($msg);
        }
    }

  function onRender()
    {
      $this->add_backup_files_rs();
    }

  /** create and add the recordset 'backup_files' */
  function add_backup_files_rs()
    {
      $rs = new EditableRS('backup_files');

      $path = APP_PATH.'db/backup/';
      if (file_exists($path))  $dir = opendir($path);
      if ($dir) 
        {
          while (($fname=readdir($dir)) !== false)
            { 
              if ($fname=='.' or $fname=='..' 
                  or $fname=='CVS' or $fname=='.svn')  continue;

              $rs->addRec(array('fname'=>$fname));
            }
          closedir($dir); 
        }

      global $webPage;
      $webPage->addRecordset($rs);
    }
}
?>