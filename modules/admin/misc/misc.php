<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class misc extends WebObject
{
  function on_refresh($event_args)
    {
      //do nothing, just display again the same page in the same state
    }

  function on_reboot($event_args)
    {
      WebApp::message(T_("Rebooting the router, please wait!"));
      shell('reboot');
    }

  function on_update_software($event_args)
    {
      set_time_limit(0);
      shell('chmod +x scripts/update.sh');
      shell('scripts/update.sh');
      WebApp::message(T_("Done"));
    }

  function on_upgrade_software($event_args)
    {
      $tag = $event_args['tag'];

      //check that the supplied tag is correct
      $tags = shell("scripts/get-tags.sh");
      $arr_tags = explode(' ', trim($tags));
      if (!in_array($tag, $arr_tags))
        {
          $msg = T_("'v_tag' does not exist!");
          $msg = str_replace('v_tag', $tag, $msg);
          WebApp::message($msg);
          return;
        }

      //upgrade
      set_time_limit(0);
      shell("scripts/upgrade.sh $tag");
      WebApp::message(T_("Done"));
    }

  function on_set_time($event_args)
    {
      $time = $event_args['time'];
      shell("date -s '$time'");
    }

  function onRender()
    {
      $time = shell_exec('date');
      $time = trim($time);
      WebApp::addVar('time', $time);
    }
}
?>
