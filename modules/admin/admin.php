<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package admin
 */
class admin extends WebObject
{
  function init()
    {
      WebApp::addSVar('module', 'clients/clients.html');
    }

  function onParse()
    {
      //get the menu items that the user can access
      include TPL.'admin/menu_items.php';

      //get the selected menu item
      $menu_item = WebApp::getSVar('tabs1::menu->selected_item');

      //check the validity of the selected menu item
      $arr_items = array_keys($menu_items);
      if (!in_array($menu_item, $arr_items))
        {
          $menu_item = $arr_items[0];
          WebApp::setSVar('tabs1::menu->selected_item', $menu_item);
        }

      //select the module according to the selected menu item
      switch ($menu_item)
        {
        case 'clients':
          $module = 'clients/clients.html';
          break;
        case 'users':
          $module = 'users/users.html';
          break;
        case 'settings':
          $module = 'settings/settings.html';
          break;
        case 'misc':
          $module = 'misc/misc.html';
          break;
        default:
        case 'logs':
          $module = 'logs/logs.html';
          break;
        case 'stats':
          $module = 'stats/traffic_stats.html';
          break;
        case 'su':
          $module = 'su/su.html';
          break;
        }

      //set variables
      WebApp::setSVar('module', $module);
      WebApp::addGlobalVar('module_title', $menu_items[$menu_item]);
    }

  function onRender()
    {
    }
}
?>