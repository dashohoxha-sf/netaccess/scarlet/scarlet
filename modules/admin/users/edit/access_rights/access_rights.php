<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package users
 */
class access_rights extends WebObject
{
  function on_save_acc_rights($event_args)
    {
      //modify access rights
      $interfaces = $event_args['access_rights'];

      //update user data
      WebApp::execDBCmd('update_access_rights', $event_args);
    }

  function onRender()
    {
      //get the access rights of the user
      $rs = WebApp::openRS('get_user_access_rights');
      $access_rights = $rs->Field('access_rights');
      $user_acc_rights = explode(',', $access_rights);

      //build the recordset of the access rights and add it to the webPage
      include TPL.'admin/menu_items.php';
      $rs = new EditableRS('access_rights');
      while ( list($id,$title) = each($menu_items) )
        {
          if ($id=='su')  continue;  //don't show the su tab
          $checked = (in_array($id, $user_acc_rights) ? 'true' : 'false');
          $rs->addRec(compact('id', 'title', 'checked'));
        }
      global $webPage;
      $webPage->addRecordset($rs);
    }
}
?>