<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/** 
 * The $menu_items array contains the items of the tabs1. 
 * @package site
 */

//an array of all the available interfaces of the program
$interfaces = array(
                    'clients'  => T_("Clients"),
                    'users'    => T_("Users"),
                    'settings' => T_("Settings"),
                    'misc'     => T_("Admin"),
                    'logs'     => T_("Logs"),
                    'stats'    => T_("Stats"),
                    );

/*
if (UNREGISTERED=='true')
{
  if (SU=='true')
    {
      //even for an unregistered copy, the superuser can access 
      //all the interfaces, except of clients
      unset($interfaces['clients']);
    }
  else
    {
      //if UNREGISTERED and not SU, then just access the logs interface
      $interfaces = array('logs' => T_("Logs"));
    }
}
*/

//get user access rights
$access_rights = WebApp::getSVar('access_rights');
$arr_access_rights = explode(',', $access_rights);

//build the array menu_items, which contains only the
//interfaces that are allowed for this user (superuser has them all)
$menu_items = array();
while (list($id, $title) = each($interfaces))
{
  if (SU=='true' or in_array($id, $arr_access_rights))
    {
      $menu_items[$id] = $title;
    }
}

//add an item (interface) that is available only to the superuser
if (SU=='true')  $menu_items['su'] = T_("SU"); 
?>