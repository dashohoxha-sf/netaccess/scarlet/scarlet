// -*-C-*-
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function refresh()
{
  SendEvent('settings', 'refresh');
}

function get_default_values()
{
  SendEvent('settings', 'get_default_values');
}

function get_current_values()
{
  SendEvent('settings', 'get_current_values');
}

function save()
{
  var form = document.settings;
  if (!validate(form))  return;  
  var event_args = getEventArgs(form);

  SendEvent('settings', 'save', event_args);
}

function validate(form)
{
  var form = document.settings;
  var section = session.getVar('tabs2::settings->selected_item');

  switch (section)
    {
    case 'wan':
      if (form.DNS1.value=='')
        {
          alert(T_("Please don't leave empty DNS1."));
          form.DNS1.focus();
          return false;
        }
      break;
    }

  return true;
}

function test()
{
  var msg = T_("After applying the new settings for a certain time, \n\
the old settings will be applied again.\n\
Enter the test time (in seconds):");
  time = prompt(msg, 300);
  if (time==null)  return;

  SendEvent('settings', 'test', 'time='+time);
}

function apply()
{
  var msg = T_("After applying the new settings the system may become \n\
unstable/unreachable, so it is advisable to test them first.\n\
\n\
Are you sure you want to apply these settings?");

  if (confirm(msg))  SendEvent('settings', 'apply');
}
