<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


include_once FORM_PATH.'formWebObj.php';

/**
 * @package settings
 */
class settings extends formWebObj
{
  function on_save($event_args)
    {
      //find out which section (tab) of settings is being saved 
      //(cab be: wan, ports, lan, bandwidth)
      $section = WebApp::getSVar('tabs2::settings->selected_item');

      if ($section=='ports')
        {
          //remove new lines from PORT_LIST
          $port_list = $event_args['PORT_LIST'];
          $port_list = str_replace("\r\n", ' ', $port_list);
          $event_args['PORT_LIST'] = $port_list;
        }

      //check that the values are valid
      if (!$this->validate($section, $event_args))  return;

      //delete the current settings
      $arr_settings = array_keys($event_args);
      $list_settings = "'" . implode("', '", $arr_settings) . "'";
      $query = "DELETE FROM settings WHERE name IN ($list_settings)";
      WebApp::execQuery($query);

      //save the new settings
      reset($event_args);
      while (list($name,$value) = each($event_args))
        {
          $arr_values[] = "('$section', '$name', '$value')";
        }
      $values = implode(', ', $arr_values);
      $query = "INSERT INTO settings (section, name, value) VALUES $values";
      WebApp::execQuery($query);
    }

  /** return true only if the values of the settings are correct */
  function validate($section, $arr_settings)
    {
      switch ($section)
        {
        case 'wan': 
          return $this->validate_wan($arr_settings); 
          break;
        }

      return true;
    }

  /** return true only if the values of the WAN settings are correct */
  function validate_wan($arr_settings)
    {
      $valid = true;

      //check that netmask is not >31
      $arr_vars = array('WAN_IP1', 'WAN_IP2', 
                        'LAN_IP1', 'LAN_IP2', 'LAN_IP3');
      foreach ($arr_vars as $IP)
        {
          $arr = explode('/', $arr_settings[$IP]);
          $netmask = $arr[1];
          if ($netmask > 31)
            {
              WebApp::message(T_("Netmask of $IP is not OK."));
              $valid = false;
            }
        }

      //check that gateway and IP are on the same network
      $ip1 = $arr_settings['WAN_IP1'];
      $ip2 = $arr_settings['WAN_IP2'];
      $gate1 = $arr_settings['GATEWAY1'];
      $gate2 = $arr_settings['GATEWAY2'];

      $ip1 = preg_replace('#\.\d+/\d+$#', '', $ip1);
      $ip2 = preg_replace('#\.\d+/\d+$#', '', $ip2);
      $gate1 = preg_replace('#\.\d+$#', '', $gate1);
      $gate2 = preg_replace('#\.\d+$#', '', $gate2);

      if ($ip1 != $gate1)
        {
          WebApp::message(T_("WAN_IP1 and GATEWAY1 do not match!"));
          $valid = false;
        }
      if ($ip2 != $gate2)
        {
          WebApp::message(T_("WAN_IP2 and GATEWAY2 do not match!"));
          $valid = false;
        }

      return $valid;
    }

  /** save the values from the database to the config files and apply them */
  function on_apply($event_args)
    {
      $arr_settings = $this->get_settings();
      $arr_settings['G2_PORTS']  = '"' . $arr_settings['G2_PORTS'] . '"';
      $arr_settings['PORT_LIST'] = '"' . $arr_settings['PORT_LIST'] . '"';

      $settings = '';
      while (list($name,$value) = each($arr_settings))
        {
          $settings .= "$name=$value\n";
        }
      $settings .= "\nDOMAIN=localnet.net\n";
      $settings .= "ACCESS_IP=".ACCESS_IP."\n";

      //get the name of the config file
      $fname = APP_PATH.'server-config/gateway.cfg';

      //backup the config file (in any case)
      shell("cp -f $fname $fname.bak");

      //add a log record
      $user = WebApp::getSVar('username');
      $details = "Source=admin, Admin=$user, Action=apply, Comment: $settings";
      log_event('~settings', $details);

      //write the new settings to the config file and reconfig the server
      write_file($fname, $settings);
      shell('server-config/reconfig.sh');
    }

  function on_get_current_values($event_args)
    {
      $this->update_settings($this->get_current_settings());
    }

  function on_get_default_values($event_args)
    {
      $arr_settings = array('WAN_IP1'        => '192.168.0.250/24',
                            'GATEWAY1'       => '192.168.0.1',
                            'WAN_IP2'        => '192.168.100.250/24',
                            'GATEWAY2'       => '192.168.100.1',
                            'G2_PORTS'       => '22 25',
                            'DNS1'           => '192.168.0.1',
                            'DNS2'           => '192.168.100.1',
                            'DNS3'           => '',
                            'PORT_LIST'      => '1755 554 7070 6699 6700 41000:41900 1080',
                            'OPEN_PORTS'     => 'false',
                            'LAN_IP1'        => '192.168.10.1/24',
                            'LAN_IP2'        => '',
                            'LAN_IP3'        => '',
                            'UPLOAD_LIMIT'   => '300',
                            'DOWNLOAD_LIMIT' => '750',
                            'ENABLE_SQUID'   => 'true',
                            'ENABLE_DHCPD'   => 'true'
                            );
      $this->update_settings($arr_settings);
    }

  function update_settings($arr_settings)
    {
      while (list($name,$value) = each($arr_settings))
        {
          $query = "UPDATE settings SET value='$value' WHERE name='$name'";
          WebApp::execQuery($query);
        }
    }

  /** test the new settings for a short time */
  function on_test($event_args)
    {
      $time = $event_args['time'];
      $arr_settings = $this->get_settings();
      $arr_settings['G2_PORTS']  = '"' . $arr_settings['G2_PORTS'] . '"';
      $arr_settings['PORT_LIST'] = '"' . $arr_settings['PORT_LIST'] . '"';

      $settings = '';
      while (list($name,$value) = each($arr_settings))
        {
          $settings .= "$name=$value\n";
        }
      $settings .= "\nDOMAIN=localnet.net\n";
      $settings .= "ACCESS_IP=".ACCESS_IP."\n";

      //add a log record
      $user = WebApp::getSVar('username');
      $details = "Source=admin, Admin=$user, Action=test, "
        . "Comment: time=$time $settings";
      log_event('~settings', $details);

      //apply the settings for the given time
      $fname = APP_PATH.'server-config/gateway.cfg.test';
      write_file($fname, $settings);
      $output = shell("nohup server-config/test.sh $time");
    }

  function onRender()
    {
      WebApp::addVars($this->get_settings());

      //current_settings
      $arr_settings = $this->get_current_settings();
      while (list($name, $value) = each($arr_settings) )
        {
          $arr_curr_settings['CURRENT_'.$name] = $value;
        }
      WebApp::addVars($arr_curr_settings);
    }

  /** returns an associative array with the settings and their values */
  function get_settings()
    {
      $arr_settings = array();
      $query = 'SELECT * FROM settings ORDER BY section, name';
      $rs = WebApp::execQuery($query);
      while (!$rs->EOF())
        {
          $name = $rs->Field('name');
          $value = $rs->Field('value');
          $arr_settings[$name] = $value;
          $rs->MoveNext();
        }

      return $arr_settings;
    }

  /** returns an associative array with the current settings */
  function get_current_settings()
    {
      $arr_settings = array();
      $arr_lines = file(APP_PATH."server-config/gateway.cfg");
      for ($i=0; $i < sizeof($arr_lines); $i++)
        {
          $line = trim($arr_lines[$i]);
          if ($line=='')  continue;
          if ($line[0]=='#') continue;
          list($name,$value) = split('=', $line, 2);
          $arr_settings[$name] = $value;
        }
      //remove quotes from G2_PORTS
      $g2_ports = trim($arr_settings['G2_PORTS']);
      $g2_ports = substr($g2_ports, 1, strlen($g2_ports)-2);
      $arr_settings['G2_PORTS'] = $g2_ports;
      //remove quotes from PORT_LIST
      $port_list = trim($arr_settings['PORT_LIST']);
      $port_list = substr($port_list, 1, strlen($port_list)-2);
      $arr_settings['PORT_LIST'] = $port_list;

      return $arr_settings;
    }
}
?>