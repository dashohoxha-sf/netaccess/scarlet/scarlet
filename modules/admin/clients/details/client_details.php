<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once FORM_PATH.'formWebObj.php';

/**
 * @package    clients
 */
class client_details extends formWebObj
{
  function init()
    {
      $this->addSVar('mode', 'add');    // add | edit
    }

  function on_refresh()
    {
      //do nothing
    }

  /** add the new client in database */
  function on_add($event_args)
    {
      //check that such a client does not exist in DB
      $client = $event_args['client'];
      $rs = WebApp::openRS('get_client', compact('client'));
      if (!$rs->EOF())
        {
          $msg = T_("The username 'var_client' is already used for \n"
            . "somebody else.  Please choose another username.");
          $msg = str_replace('var_client', $client, $msg);
          WebApp::message($msg);
          return;
        }

      //add the client
      $event_args['expiration_time'] = date('Y-m-d H:i');
      $this->insert_record($event_args, 'clients');

      //set the new client as current client and change the mode to edit
      WebApp::setSVar('clientList->current_client', $client);
      $this->setSVar('mode', 'edit');

      //add a log record
      $user = WebApp::getSVar('username');
      log_event('+client', "Source=admin, Admin=$user, Client=$client");

      //reinit the clients of the script 'check-traffic-limits.php'
      shell('touch cron/reinit-clients.flag');
    }

  /** delete the current client */
  function on_delete($event_args)
    {
      //these will be used to log the client deletion
      $client = WebApp::getSVar('clientList->current_client');
      $rs_macs = WebApp::openRS('get_client_macs', compact('client'));

      //delete the client and his macs
      WebApp::execDBCmd('delete_client');
      WebApp::execDBCmd('delete_client_macs');

      //remove client macs
      $this->remove_client_macs($rs_macs);

      //the current_client is deleted,
      //set current the first client in the list
      $clientList = WebApp::getObject('clientList');
      $clientList->selectFirst();

      //acknowledgment message
      WebApp::message(T_("Client deleted."));

      //add log records
      $this->log_client_deletion($client, $rs_macs);

      //reinit the clients of the script 'check-traffic-limits.php'
      shell('touch cron/reinit-clients.flag');
    }

  /** block the macs in the firewall and remove their counter files */
  function remove_client_macs($rs_macs)
    {
      while (!$rs_macs->EOF())
        {
          $mac = $rs_macs->Field('mac');

          //stop the connection
          shell("server-config/firewall/mac-deny.sh $mac");

          //remove the rrd file of the mac
          $mac1 = str_replace(':', '', $mac);
          $mac1 = str_replace('-', '', $mac1);
          shell("rm server-config/traffic-counter/rrd/mac_$mac1");

          $rs_macs->MoveNext();
        }      
    }

  /** add log records about the client deletion */
  function log_client_deletion($client, $rs_macs)
    {
      $user = WebApp::getSVar('username');

      //log client deletion
      log_event('-client', "Source=admin, Admin=$user, Client=$client");

      //log mac deletions
      while (!$rs_macs->EOF())
        {
          $mac = $rs_macs->Field('mac');
          $details = "Source=admin, Admin=$user, Client=$client, MAC=$mac, "
            . "Comment: client deletion";
          log_event('-MAC', $details);
          $rs_macs->MoveNext();
        }
    }

  /** save the changes */
  function on_save($event_args)
    {
      //update client data
      $record = $event_args;
      $record['client'] = WebApp::getSVar('clientList->current_client');

      //get the tab that is selected
      $tab = WebApp::getSVar('tabs2::client->selected_item');

      if ($tab=='connection')
        {
          $time = $record['expiration_time'];
          $record['expiration_time'] = date('Y-m-d H:i', strtotime($time));
        }

      //update the record
      $this->update_record($record, 'clients', 'client');

      //add a log record if the connection details are updated
      if ($tab=='connection')
        {
          $user = WebApp::getSVar('username');
          $client = $record['client'];
          $expiration = $record['expiration_time'];
          $nr_conn = $record['nr_connections'];
          $passwd = $record['password'];
          $details = "Source=admin, Admin=$user, Client=$client, "
            . "Comment: $nr_conn $passwd $expiration";
          log_event('~client', $details);
        }
    }

  function onParse()
    {
      //get the current client from the list of clients
      $client = WebApp::getSVar('clientList->current_client');

      //set mode 
      if ($client==UNDEFINED)
        {
          $this->setSVar('mode', 'add');
        }
      else
        {
          $this->setSVar('mode', 'edit');
        }
    }

  function onRender()
    {
      $mode = $this->getSVar('mode');

      if ($mode=='add')
        {
          $client_data = $this->pad_record(array(), 'clients');
          $client_data['expiration_time'] =  date('Y-m-d H:i');
        }
      else
        {
          $rs = WebApp::openRS('current_client');
          $client_data = $rs->Fields();
        }
      $time = $client_data['expiration_time'];
      $client_data['expiration_time'] = date('Y-m-d H:i', strtotime($time));
      WebApp::addVars($client_data);  

      //add current_time
      $current_time = date('Y-m-d H:i');
      WebApp::addVar('current_time', $current_time);
    }
}
?>