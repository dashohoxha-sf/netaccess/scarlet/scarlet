<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    clients
 */
class select_client_details extends WebObject
{
  function onParse()
    {
      global $event;
      if ($event->target=='clientList' and $event->name=='add_new_client')
        {
          WebApp::setSVar('tabs2::client->selected_item', 'contacts');
        }

      //select which client details to display
      $tab = WebApp::getSVar('tabs2::client->selected_item');
      switch ($tab)
        {
        default:
        case 'contacts':
          $details = 'contacts.html';
          break;
        case 'connection':
          $details = 'connection.html';
          break;
        case 'stats':
          $details = 'stats/client_stats.html';
          break;
        }
      WebApp::addVar('details', $details);
    }
}
?>