<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    clients
 * @subpackage stats
 */
class graphs extends WebObject
{
  function init()
    {
      $client = client_stats::get_client();
      $this->addSVar('client', $client);
      $this->addSVar('mac', 'all_macs');
      $this->addSVar('time', '2days');  //10minutes|2hours|2days|1month|8months
    }

  function on_refresh_graph($event_args)
    {
      $this->setSVar('mac', $event_args['mac']);
      $this->setSVar('time', $event_args['time']);
    }

  function onParse()
    {
      $client = client_stats::get_client();
      $current_client = $this->getSVar('client');
      if ($client!=$current_client)
        {
          $this->setSVar('client', $client);
          $this->setSVar('mac', 'all_macs');
        }
    }

  function onRender()
    {
      $this->add_listbox_macs();
      $this->add_listbox_times();
      $this->make_graph();
    }

  /** create the recordset 'listbox::macs' and add it to the webPage */
  function add_listbox_macs()
    {
      $rs = new EditableRS('listbox::macs');
      $rs->addRec(array('id'=>'all_macs', 'label'=>T_("All MACs")));

      $macs_rs = WebApp::openRS('client_stats->macs');
      while (!$macs_rs->EOF())
        {
          $mac = $macs_rs->Field('mac');
          $name = $macs_rs->Field('hostname');
          $rs->addRec(array('id'=>$mac, 'label'=>"$mac ($name)"));
          $macs_rs->MoveNext();
        }

      global $webPage;
      $webPage->addRecordset($rs);
    }

  /** create the recordset 'listbox::times' and add it to the webPage */
  function add_listbox_times()
    {
      $rs = new EditableRS('listbox::times');

      $rs->addRec(array('id'=>'10minutes', 'label'=>T_("Last 10 Minutes")));
      $rs->addRec(array('id'=>'2hours',    'label'=>T_("Last 2 Hours")));
      $rs->addRec(array('id'=>'2days',     'label'=>T_("Last 2 Days")));
      $rs->addRec(array('id'=>'1month',    'label'=>T_("Last Month")));
      $rs->addRec(array('id'=>'8months',   'label'=>T_("Last 8 Months")));

      global $webPage;
      $webPage->addRecordset($rs);
    }

  /** generate the graph and save it to a file in the tmp/ directory */
  function make_graph()
    {
      $client = $this->getSVar('client');
      $mac = $this->getSVar('mac');
      $interval = $this->getSVar('time');

      //get a list of the macs which will be graphed
      if ($mac=='all_macs')
        {
          $rs = WebApp::openRS('client_stats->macs');
          $arr_macs = $rs->getColumn('mac');
        }
      else
        {
          $arr_macs = array($mac);
        }
      $mac_list = implode(' ', $arr_macs);

      //the file where the image of the graph will be outputed
      $file = dirname(__FILE__)."/tmp/${client}.png";

      //call the script that makes the graph
      $rrdgraph = 'server-config/traffic-counter/rrdgraph.sh';
      $cmd = "$rrdgraph $file $interval $mac_list";
      $output = shell($cmd);
      //WebApp::debug_msg("<xmp>$cmd\n$output</xmp>");
    }
}
?>