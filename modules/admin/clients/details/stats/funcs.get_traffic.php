<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * Find and return the traffic, in the format array($upload, $download)
 * for the given mac and in the interval between the starttime and endtime
 * timestamps. If the resolution parameter is not given, then the 
 * resolution of 1 day (86400s) is taken by default.
 */
function get_mac_traffic($mac, $starttime, $endtime, $resolution =86400)
{
  $upload = 0.0;
  $download = 0.0;

  $mac = str_replace(':', '', $mac);
  $mac = str_replace('-', '', $mac);

  $rrd_file = "server-config/traffic-counter/rrd/mac_$mac";
  if (!file_exists($rrd_file))  return array($upload, $download);

  $rrdfetch = "rrdtool fetch $rrd_file AVERAGE "
    . " --resolution $resolution --start $starttime --end $endtime";
  $result = shell("$rrdfetch | sed '1,2 d'");


  $arr_lines = explode("\n", $result);
  for ($i=0; $i < sizeof($arr_lines); $i++)
    {
      $line = trim($arr_lines[$i]);
      if ($line=='')  continue;
      $arr = explode(' ', $line);
      $download += $arr[1];
      $upload += $arr[2];
    }

  $upload *= $resolution;
  $download *= $resolution;

  return array($upload, $download);
}

/**
 * Find and return the traffic, in the format array($upload,$download),
 * for the given mac from the starttime  and up to the current time.
 * Different resolutions are used, in order to get the most accurate value
 * up to the current time.
 */
function get_current_traffic($mac, $starttime, $now =UNDEFINED)
{
  if ($now==UNDEFINED)  $now = time();

  $upload = 0.0;
  $download = 0.0;

  $interval = $now - $starttime;

  //if interval is longer than 1 day
  $day = 86400;  //number of secs in 1 day
  if ($interval > $day)
    {
      $start = floor($starttime / $day) * $day;
      $end = floor($now / $day) * $day;
      list($up, $down) = get_mac_traffic($mac, $start, $end, $day);
      $upload += $up;
      $download += $down;
      $starttime = $end;
      $interval = $now - $starttime;
    }

  //if interval is longer than 1 hour
  $hour = 3600;  //number of secs in 1 hour
  if ($interval > $hour)
    {
      $start = floor($starttime / $hour) * $hour;
      $end = floor($now / $hour) * $hour;
      list($up, $down) = get_mac_traffic($mac, $start, $end, $hour);
      $upload += $up;
      $download += $down;
      $starttime = $end;
      $interval = $now - $starttime;
    }

  //if interval is longer than 5min
  $res = 300;  //number of secs in 5min
  if ($interval > $res)
    {
      $start = round($starttime / $res) * $res;
      $end = floor($now / $res) * $res;
      list($up, $down) = get_mac_traffic($mac, $start, $end, $res);
      $upload += $up;
      $download += $down;
      $starttime = $end;
      $interval = $now - $starttime;
    }

  //interval is shorter than 5min
  $res = 10;
  $start = floor($starttime / $res) * $res;
  $end = floor($now / $res) * $res;
  list($up, $down) = get_mac_traffic($mac, $start, $end, $res);
  $upload += $up;
  $download += $down;

  return array($upload,$download);
}
?>