<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once dirname(__FILE__).'/funcs.get_traffic.php';

/**
 * @package    clients
 * @subpackage stats
 */
class client_stats extends WebObject
{
  /** return the client id */
  function get_client()
    {
      //in case that we are in admin interface
      $client = WebApp::getSVar('clientList->current_client');

      //maybe we are in connect interface
      if ($client==UNDEFINED)  $client = WebApp::getSVar('connect->username');

      return $client;
    }

  function onParse()
    {
      $client = $this->get_client();
      $rs = WebApp::openRS('client_stats->macs', compact('client'));
      $this->build_months_rs();
    }

  /**
   * Creates a recordset with the last 6 months and the current month.
   * For each month it has the field 'name' with the short name of the month
   * (e.g. 'Jan', 'Feb', etc.), and the field 'starttime' with the 
   * unix timestamp of the start of the month. Adds it to the webPage.
   */
  function build_months_rs()
    {
      $months = new EditableRS('months');
      $curr_month = date('n');
      $curr_year = date('Y');
      $day = 24*60*60;  //number of secs in one day
      for ($i=0; $i < 7; $i++)
        {
          $mon = $curr_month - $i;
          $starttime = mktime(0,0,0,$mon,1,$curr_year);
          $month_id = date('M', $starttime);
          $month_name = T_($month_id);
          $rec = array(
                       'id'        => $month_id, 
                       'name'      => $month_name, 
                       'starttime' => round($starttime/$day)*$day
                       );
          $months->insRec($rec);
        }

      global $webPage;
      $webPage->addRecordset($months);

      return;

      // These are for translation purposes
      $str = T_("Jan").T_("Feb").T_("Mar").T_("Apr").T_("May").T_("Jun");
      $str = T_("Jul").T_("Aug").T_("Sep").T_("Oct").T_("Nov").T_("Dec");
    }

  function onRender()
    {
      //global $webPage;

      //$webPage->timer->Start('build_traffic_rs_from_rrd', 'build_traffic_rs_from_rrd');
      //$this->build_traffic_rs_from_rrd();
      //$webPage->timer->Stop('build_traffic_rs_from_rrd');

      //$webPage->timer->Start('build_traffic_rs', 'build_traffic_rs');
      $this->build_traffic_rs();
      //$webPage->timer->Stop('build_traffic_rs');

      $this->add_totals();
    }

  /**
   * Builds and adds to the webPage a recordset with the traffic usage
   * for each mac of the client and for each month.
   */
  function build_traffic_rs_from_rrd()
    {
      $traffic_rs = new EditableRS('traffic_rs');

      $macs = WebApp::openRS('client_stats->macs');
      $months = WebApp::openRS('months');
      $arr_months = $months->getColumn('id');
      $arr_times = $months->getColumn('starttime');

      //for each mac, find the traffic consumption for each month
      //and set it in the recordset traffic_rs
      while (!$macs->EOF())
        {
          $mac_traffic = $macs->Fields();
          $mac = $mac_traffic['mac'];

          //calculate the traffic for the last 6 months
          for ($i=0; $i < sizeof($arr_months); $i++)
            {
              $mon = $arr_months[$i];
              $starttime = $arr_times[$i];
              $endtime = $arr_times[$i+1];

              if ($i==sizeof($arr_months)-1)
                {
                  list($upload, $download) = 
                    get_current_traffic($mac, $starttime);
                }
              else
                {
                  list($upload, $download) = 
                    get_mac_traffic($mac, $starttime, $endtime);
                }

              //format the values of $upload and $download to MBytes,
              //with one digit after the decimal point 
              $upload = round($upload / 1024) / 1024.0;
              $upload = number_format($upload, 1);
              $download = round($download / 1024) / 1024.0;
              $download = number_format($download, 1);
              $mac_traffic[$mon] = "$upload / $download";
            }

          //add the recordset of this mac to traffic_rs
          $traffic_rs->addRec($mac_traffic);

          $macs->MoveNext();
        }

      global $webPage;
      $webPage->addRecordset($traffic_rs);
    }

  /**
   * Builds and adds to the webPage a recordset with the traffic usage
   * for each mac of the client and for each month.
   */
  function build_traffic_rs()
    {
      $traffic_rs = new EditableRS('traffic_rs');

      $months = WebApp::openRS('months');
      $arr_months = $months->getColumn('id');
      $arr_times = $months->getColumn('starttime');
      
      $date1 = date('Y-m-d', $arr_times[0]);
      $date2 = date('Y-m-d', $arr_times[sizeof($arr_times)-1]);

      //for each mac, find the traffic consumption for each month
      //and set it in the recordset traffic_rs
      $client = $this->get_client();
      $macs = WebApp::openRS('client_stats->macs', compact('client'));
      while (!$macs->EOF())
        {
          $mac = $macs->Field('mac');

          $params = compact('client', 'mac', 'date1', 'date2');
          $mac_logs = WebApp::openRS('get_mac_logs', $params);
          //WebApp::debug_msg($mac_logs->toHtmlTable());  //debug

          //calculate the traffic for the last months from the traffic logs
          $arr_traffic = array();
          $mac_logs->MoveFirst();
          for ($i=0; $i < sizeof($arr_months)-1; $i++)
            {
              $upload = 0.0;
              $download = 0.0;
              $endtime = $arr_times[$i+1];
              while (!$mac_logs->EOF() and 
                     strtotime($mac_logs->Field('date')) < $endtime)
                {
                  $upload += $mac_logs->Field('upload');
                  $download += $mac_logs->Field('download');
                  $mac_logs->MoveNext();
                }
                  
              //save the calculated upload/download
              $mon = $arr_months[$i];
              $arr_traffic[$mon] = "$upload/$download";
            }

          //calculate the traffic for the current month and save it
          $starttime = $arr_times[sizeof($arr_months)-1];
          list($upload, $download) = get_current_traffic($mac, $starttime);
          $upload = round($upload / 1024.0) / 1024.0;
          $download = round($download / 1024.0) / 1024.0;
          $mon = $arr_months[sizeof($arr_months)-1];
          $arr_traffic[$mon] = "$upload/$download";

          //format the values of $upload and $download to MBytes,
          //with one digit after the decimal point 
          $mac_traffic = $macs->Fields();
          while (list($mon, $traffic) = each($arr_traffic))
            {
              list($upload,$download) = explode('/', $traffic);
              $upload = number_format($upload, 1);
              $download = number_format($download, 1);
              $mac_traffic[$mon] = "$upload / $download";
            }

          //add the recordset of this mac to traffic_rs
          $traffic_rs->addRec($mac_traffic);

          $macs->MoveNext();
        }

      global $webPage;
      $webPage->addRecordset($traffic_rs);
    }

  /**
   * Find the total traffic for each month and add it to the web page.
   */
  function add_totals()
    {
      $months = WebApp::openRS('months');
      $traffic_rs = WebApp::openRS('traffic_rs');

      while (!$months->EOF())
        {
          $mon = $months->Field('id');
          $total_upload = 0.0;
          $total_download = 0.0;

          $arr_month_traffic = $traffic_rs->getColumn($mon);
          for ($i=0; $i < sizeof($arr_month_traffic); $i++)
            {
              $traffic = $arr_month_traffic[$i];
              list($upload,$download) = explode('/', $traffic);
              $upload = str_replace(',', '', trim($upload));
              $download = str_replace(',', '', trim($download));
              $total_upload += $upload;
              $total_download += $download;
            }

          //add the variable of totals for the month
          $total_upload = number_format($total_upload, 1);
          $total_download = number_format($total_download, 1);
          WebApp::addVar("total_$mon", "$total_upload / $total_download");

          $months->MoveNext();
        }      
    }
}
?>