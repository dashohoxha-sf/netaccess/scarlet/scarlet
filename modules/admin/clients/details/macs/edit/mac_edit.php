<?php
/*
This file  is part  of HGSM.   HGSM is a  web application  for keeping
information about a hierarchical structure (in this case a grid).

Copyright 2005, 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

HGSM is free software; you  can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

HGSM is  distributed in the hope  that it will be  useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with HGSM; if not,  write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


/**
 * @package clients
 */
class mac_edit extends WebObject
{
  var $mac_record = array(
                           'mac'       => '',
                           'connected' => 'false',
                           'timestamp' => '',
                           'hostname'  => ''
                           );
  
  function init()
    {
      $this->addSVar('mode', 'hidden');  // add | edit | hidden
      $this->addSVar('mac', UNDEFINED);
    }

  function on_save($event_args)
    {
      $mode = $this->getSVar('mode');
      $event_args['client'] = WebApp::getSVar('clientList->current_client');
      $event_args['timestamp'] = time();

      if ($mode=='add')
        {
          $this->add_mac($event_args);
        }
      else if ($mode=='edit')
        {
          WebApp::execDBCmd('update_mac', $event_args);

          //editing mac is done
          $this->setSVar('mode', 'hidden');
        }

      $mac = $event_args['mac'];
      $connected = $event_args['connected'];
      $this->update_field_connected($mac, $connected);
    }

  function add_mac($record)
    {
      //check that mac does not exist in the table
      $rs = WebApp::openRS('get_mac', $record);
      if (!$rs->EOF())
        {
          $this->mac_record = $record;
          $mac = $record['mac'];
          $msg = T_("MAC 'v_mac' is already used.");
          $msg = str_replace('v_mac', $mac, $msg);
          WebApp::message($msg);
          return;
        }

      //add the new mac
      WebApp::execDBCmd('add_mac', $record);

      //switch the editing mode to hidden
      $this->setSVar('mode', 'hidden');

      //log the event
      $user = WebApp::getSVar('username');
      $d = "Source=admin, Admin=$user, MAC=$mac, Comment: adding";
      log_event('+MAC', $d);
    }

  /**
   * Update the field connected of the given mac in the table 'macs',
   * but only if such a thing is possible (e.g. client limits have not
   * been reached). Also connects/disconnects the mac and records it in logs. 
   */
  function update_field_connected($mac, $connected)
    {
      //get the previous value of the field 'connected'
      $rs = WebApp::openRS('get_mac', compact('mac'));
      $prev_connected = $rs->Field('connected');

      //if the new value is the same as the previous one
      //there is no need to do anything
      if ($prev_connected==$connected)  return;
          
      if ($connected=='false')
        {
          //update the field in DB
          $args = compact('mac', 'connected');
          WebApp::execDBCmd('update_field_connected', $args);

          //deny this MAC in the firewall
          $path = APP_PATH."server-config/firewall";
          shell("$path/mac-deny.sh $mac");

          //log the event
          $user = WebApp::getSVar('username');
          $d = "Source=admin, Admin=$user, MAC=$mac, Comment: disconnected";
          log_event('~MAC', $d);

	  //reinit the clients of the script 'check-traffic-limits.php'
	  shell('touch cron/reinit-clients.flag');
        }
      else 
        if ($this->mac_can_be_connected($mac))
          {
            //update the field in DB
            $args = compact('mac', 'connected');
            WebApp::execDBCmd('update_field_connected', $args);

            //allow this MAC in the firewall
            $path = APP_PATH."server-config/firewall";
            shell("$path/mac-allow.sh $mac");

            //log the event
            $user = WebApp::getSVar('username');
            $d = "Source=admin, Admin=$user, MAC=$mac, Comment: connected";
            log_event('~MAC', $d);

	    //reinit the clients of the script 'check-traffic-limits.php'
	    shell('touch cron/reinit-clients.flag');
          }
    }

  /**
   * Returns true if the mac of the current client can be allowed 
   * to connect to internet. Otherwise returns false.
   */
  function mac_can_be_connected($mac)
    {
      //get the client
      $client = client_macs::get_client();

      //get the client limits
      $rs = WebApp::openRS('get_client_limits', compact('client'));
      $nr_connections = $rs->Field('nr_connections');
      $expiration_time = $rs->Field('expiration_time');
      $upload_limit = $rs->Field('upload_limit');
      $download_limit = $rs->Field('download_limit');

      //check that the expiration time has not passed
      if (strtotime($expiration_time) < time())
        {
          $msg = T_("MAC v_mac cannot be connected to internet, \n\
because the time limit of the client has expired.");
          $msg = str_replace('v_mac', $mac, $msg);
          WebApp::message($msg);
          return false;
        }

      //check that the client has available traffic left
      if (($upload_limit <= 0) or ($download_limit <=0))
	{
          $msg = T_("MAC v_mac cannot be connected to internet, \n\
because the client has no traffic available to be used.");
          $msg = str_replace('v_mac', $mac, $msg);
          WebApp::message($msg);
          return false;	  
	}

      //check that the max nr of connections is not passed
      $rs = WebApp::openRS('connected_macs', compact('client'));
      if ($rs->count >= $nr_connections)
        {
          $msg = T_("MAC v_mac cannot be connected to internet, \n\
because the maximum number of connections of the client has been reached.");
          $msg = str_replace('v_mac', $mac, $msg);
          WebApp::message($msg);
          return false;          
        }

      //all the checks are passed successfully, mac can be connected
      return true;
    }


  function on_cancel($event_args)
    {
      //switch the editing mode to hidden
      $this->setSVar('mode', 'hidden');
    }

  function onRender()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          $client = WebApp::getSVar('clientList->current_client');
          if ($client==UNDEFINED)
            {
              //the webbox is used in the client interface
              //get the hostname and the mac of the client computer
              $mac = WebApp::getSVar('client->mac');
              $hostname = WebApp::getSVar('client->computer_name');
              $this->mac_record['mac'] = $mac;
              $this->mac_record['hostname'] = $hostname;
            }
          $vars = $this->mac_record;
        }
      else if ($mode=='edit')
        {
          $args = array('mac' => $this->getSVar('mac'));
          $rs = WebApp::openRS('get_mac', $args);
          $vars = $rs->Fields();
        }

      WebApp::addVars($vars);
    }
}
?>