<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    clients
 */
class client_macs extends WebObject
{
  function on_del($event_args)
    {
      $mac = $event_args['mac'];

      //delete the mac from 'macs'
      WebApp::execDBCmd('del_mac', compact('mac'));

      //deny this MAC in the firewall
      $path = APP_PATH."server-config/firewall";
      shell("$path/mac-deny.sh $mac");

      //log the event
      $user = WebApp::getSVar('username');
      $d = "Source=admin, Admin=$user, MAC=$mac, Comment: deletion";
      log_event('-MAC', $d);

      //reinit the clients of the script 'check-traffic-limits.php'
      shell('touch cron/reinit-clients.flag');

      //delete also the RRD traffic stats database for this mac
      $mac1 = str_replace(':', '', $mac);
      $mac1 = str_replace('-', '', $mac1);
      $rrd_file = APP_PATH."server-config/traffic-counter/rrd/mac_$mac1";
      shell("rm $rrd_file");
    }

  function on_add($event_args)
    {
      WebApp::setSVar('mac_edit->mode', 'add');
      WebApp::setSVar('mac_edit->mac', UNDEFINED);
    }

  /** return the client id */
  function get_client()
    {
      //in case that we are in admin interface
      $client = WebApp::getSVar('clientList->current_client');

      //maybe we are in client interface
      if ($client==UNDEFINED)  $client = WebApp::getSVar('client->client');

      return $client;
    }

  function on_edit($event_args)
    {
      WebApp::setSVar('mac_edit->mode', 'edit');
      WebApp::setSVar('mac_edit->mac', $event_args['mac']);
    }

  function onRender()
    {
      $rs = WebApp::openRS('client_macs');
      $rs->apply('macs_format');
      global $webPage;
      $webPage->addRecordset($rs);
    }
}

/** display the macs fields in a propper format */
function macs_format(&$rec)
  {
    $timestamp = $rec['timestamp'];
    $rec['timestamp'] = date('Y-m-d H:i:s', $timestamp);
  }
?>