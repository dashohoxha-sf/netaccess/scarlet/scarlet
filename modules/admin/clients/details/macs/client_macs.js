// -*-C-*- //tell emacs to use C mode
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function edit_mac(mac)
{
  SendEvent('client_macs', 'edit', 'mac='+mac);
  return;
}

function add_mac()
{
  SendEvent('client_macs', 'add');
}

function del_mac(mac)
{
  var msg = T_("You are deleting the computer with mac: v_mac");
  msg = msg.replace(/v_mac/, mac);
  if (confirm(msg))
    SendEvent('client_macs', 'del', 'mac='+mac);
}
