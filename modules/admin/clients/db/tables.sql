
DROP TABLE IF EXISTS clients;
CREATE TABLE clients (
  client varchar(20) default NULL,
  firstname varchar(20) NOT NULL default '',
  lastname varchar(20) default NULL,
  e_mail varchar(25) default NULL,
  phone1 varchar(20) default NULL,
  phone2 varchar(20) default NULL,
  address varchar(35) default NULL,
  notes text,
  PRIMARY KEY  (client)
) TYPE=MyISAM;

