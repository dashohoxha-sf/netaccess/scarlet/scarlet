<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    clients
 * @subpackage clients
 */
class clientList extends WebObject
{
  function init()
    {
      $this->addSVar("current_client", UNDEFINED);      
      //set current the first client in the list
      $this->selectFirst();  
    }

  /** set current_client as the first client in the list */
  function selectFirst()
    {  
      $rs = WebApp::openRS("selected_clients");
      $first_client = $rs->Field("client");
      $this->setSVar("current_client", $first_client);
    }

  function on_next($event_args)
    {
      $page = $event_args["page"];
      WebApp::setSVar("selected_clients->current_page", $page);
      $this->selectFirst();  
    }

  function on_refresh($event_args)
    {
      //recount the selected clients
      WebApp::setSVar("selected_clients->recount", "true");

      //select the first one in the current page
      $this->selectFirst();
    }

  function on_select($event_args)
    {
      //set current_client to the selected one
      $rs = WebApp::openRS("get_client", $event_args);
      if ($rs->EOF())
        {
          $this->selectFirst();   
        }
      else
        {
          $client = $event_args["client"];
          $this->setSVar("current_client", $client);
        }
    }

  function on_add_new_client($event_args)
    {
      //when the button Add New client is clicked
      //make current_client UNDEFINED
      $this->setSVar("current_client", UNDEFINED);
    }

  function onParse()
    {
      //recount the selected clients
      WebApp::setSVar("selected_clients->recount", "true");
    }
}
?>