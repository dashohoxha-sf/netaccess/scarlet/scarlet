<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    clients
 * @subpackage clients
 */
class clientFilter extends WebObject
{
  function init()
    {
      $this->addSVars( array(
                             'client'   => '',
                             'realname' => '',
                             'phone'    => '',
                             'email'    => ''
                             ) );
      $this->addSVar('filter_condition', '(1=1)');
    }

  function onParse()
    {
      $this->build_filter_condition();
    }

  function build_filter_condition()
    {
      //get state vars
      extract($this->getSVars());

      $arr_filters = array();
      if ($client != '')
        $arr_filters[] = '(client LIKE "%'.$client.'%")';

      if ($realname != '')
        $arr_filters[] = '(firstname LIKE "%'.$realname.'%" '
          . 'OR lastname LIKE "%'.$realname.'%")'; 

      if ($phone != '')
        $arr_filters[] = '(phone1 LIKE "%'.$phone.'%" '
          . 'OR phone2 LIKE "%'.$phone.'%")'; 

      if ($email != '')
        $arr_filters[] = '(e_mail LIKE "%'.$email.'%")';

      $filter = implode(' AND ', $arr_filters);

      if ($filter=='')  $filter = '1=1';
      $filter = '('.$filter.')';

      $this->setSVar('filter_condition', $filter);
    }  
}
?>