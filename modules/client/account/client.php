<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once FORM_PATH.'formWebObj.php';

/**
 * @package client
 */
class client extends formWebObj
{
  function init()
    {
      WebApp::setSVar('tabs2::client->selected_item', 'stats');
    }

  function on_login($event_args)
    {
      $username = $event_args["username"];
      WebApp::addSVar('client', $username, 'DB');
    }

  function on_refresh($event_args)
    {
      //do nothing
    }

  /** save the changes */
  function on_save($event_args)
    {
      //client should not be able to modify anything (for the time being)
      return;

      //update client data
      $record = $event_args;
      $record['client'] = WebApp::getSVar('client');

      //get the tab that is selected
      $tab = WebApp::getSVar('tabs2::client->selected_item');

      //update the record
      $this->update_record($record, 'clients', 'client');
    }

  function onRender()
    {
      $rs = WebApp::openRS('get_client_data');
      $client_data = $rs->Fields();

      $time = $client_data['expiration_time'];
      $client_data['expiration_time'] = date('Y-m-d H:i', strtotime($time));
      WebApp::addVars($client_data);  

      //add current_time
      $current_time = date('Y-m-d H:i');
      WebApp::addVar('current_time', $current_time);
    }
}
?>