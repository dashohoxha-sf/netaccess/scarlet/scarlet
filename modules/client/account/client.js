// -*-C-*- //tell emacs to use C mode
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


function refresh_client()
{
  SendEvent('client', 'refresh');
}

/* client should not be able to modify anything (for the time being)
function save()
{
  if (data_not_valid())  return;
  
  var event_args = getEventArgs(document.client_form);
  SendEvent('client', 'save', event_args);
}
*/

/** returns true or false after validating the data of the form */
function data_not_valid()
{
  var form = document.client_form;
    
  return false;
}
