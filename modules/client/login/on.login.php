<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

if (!valid_user())
{
  //the user is not authenticated, go to the login page
  $event->targetPage = 'client/login/login.html';
  WebApp::addGlobalVar('login_error', 'true');
  WebApp::message(T_("Wrong username or password!"));
}
else
{
  //open the client details page
  $event->targetPage = 'client/account/client.html';
  $event->target = 'client';
  $event->name = 'login';
}

/**
 * Checks if the given username and password are valid.
 * Returns true or false.
 */
function valid_user()
{
  global $event;
  $username = $event->args['username'];
  $password = $event->args['password'];

  $query = ("SELECT client, password FROM clients "
            . " WHERE client='$username' AND password='$password'");
  $rs = WebApp::execQuery($query);

  if ($rs->EOF())  return false; //query returned no records

  $crypted_passwd = $rs->Field('password');
  //$valid = ($crypted_passwd == crypt($password, $crypted_passwd));
  $valid = ($crypted_passwd == $password);

  return $valid;
}
?>