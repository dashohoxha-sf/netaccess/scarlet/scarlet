<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package client
 */
class login extends WebObject
{
  function onRender()
    {
      $query = ("SELECT firstname AS isp_name, "
                . " address, phone1 AS phone, e_mail "
                . " FROM users WHERE username = 'ISP'");
      $rs = WebApp::execQuery($query);
      if (!$rs->EOF())
        {
          WebApp::addVars($rs->Fields());
        }
      else
        {
          $arr_contacts = array(
                                'isp_name' => 'Fastlink Internet Café',
                                'address'  => 'Rr. Islam Alla 34, Tirane',
                                'phone'    => '04-253867',
                                'e_mail'   => 'info@fastlink-internet.com'
                                );
          WebApp::addVars($arr_contacts);
        }
    }
}
?>