#!/usr/bin/php -q
<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * This script is called periodically by cron (each weak or each month)
 * and it does some tasks like sending logs by email, updating, etc.
 */

if ($argc != 3)
{
  print "Usage: $argv[0] period email
where 'period' is in days
and 'email' is the e-mail address that will get the message
";
  exit(1);
}
$period = $argv[1];
$email_addr = $argv[2];

define("APP_PATH", dirname(dirname(__FILE__)).'/');
define("APP_URL", '../');
include_once APP_PATH.'webapp.php';

email_logs($period, $email_addr);
update_software();
empty_session();
cleanup_dangling_macs();
check_status();

exit(0);

/*------------------------ functions -----------------------------*/

/**
 * Get the logs of the last period and send them by email.
 */
function email_logs($period, $email_addr)
{
  //get a list of the last logs
  $query = "SELECT * FROM logs "
    . "WHERE time > DATE_SUB(NOW(), INTERVAL $period DAY)";
  $rs = WebApp::execQuery($query);

  //create the list of logs
  while (!$rs->EOF())
    {
      extract($rs->Fields());
      $log_list .= "$id) ($time) $event: $details\n";
      $rs->MoveNext();
    }

  //get ISP details
  $query = "SELECT firstname, e_mail, phone1, address "
    . "FROM users WHERE username = 'ISP'";
  $rs = WebApp::execQuery($query);
  extract($rs->Fields());

  //get details like $nr_macs, $clients, $active_clients, etc.
  $arr_stats = get_stats();
  extract($arr_stats);

  //send a message
  $date = date('Y-m-d H:i');
  $msg_body = "
ISP   : $firstname
Email : $e_mail
Phone : $phone1
Address:
$address

Currently there are $nr_macs computers having internet connection.
There are $nr_clients clients and $nr_active_clients active (unexpired) clients.

List of The Logs of The Last Period ($period days), Up To $date :

$log_list
";
  $subject = "[logs] $date $firstname";
  mail($email_addr, $subject, $msg_body);
}

/**
 * Get some statistics that are included in the email of logs.
 * Returns an associated array with the details.
 */
function get_stats()
{
  //get the number of connected computers
  $query = "SELECT COUNT(mac) AS nr_macs FROM macs WHERE connected='true'";
  $rs = WebApp::execQuery($query);
  $nr_macs = $rs->Field('nr_macs');

  //get the number of clients
  $rs = WebApp::execQuery('SELECT COUNT(client) AS nr_clients FROM clients');
  $nr_clients = $rs->Field('nr_clients');

  //get the number of active (unexpired) clients
  $query = ("SELECT COUNT(client) AS nr_active_clients FROM clients "
            . "WHERE expiration_time > NOW()");
  $rs = WebApp::execQuery($query);
  $nr_active_clients = $rs->Field('nr_active_clients');

  return compact('nr_macs', 'nr_clients', 'nr_active_clients');
}

function update_software()
{
  $path = APP_PATH;
  system("chmod +x $path/scripts/update.sh");
  system("$path/scripts/update.sh");
  system("find $path -name '*.sh' | xargs chmod +x");
}

/** empty the table session */
function empty_session()
{
  $query = "DELETE FROM session ";
  $rs = WebApp::execQuery($query);
}

/**
 * Delete from 'macs' any mac that does not belong 
 * to any of the clients in the client list (dangling mac).
 */
function cleanup_dangling_macs()
{
  //get a list of all the clients
  $clients = "SELECT client FROM clients";
  $rs = WebApp::execQuery($clients);
  $arr_clients = $rs->getColumn('client');
  $client_list = "'" . implode("', '", $arr_clients) . "'";

  //get a list of the dangling macs (for logging purposes)
  $get_macs = ("SELECT client, mac FROM macs "
               . "WHERE client NOT IN ($client_list)");
  $rs = WebApp::execQuery($get_macs);

  //clean up dangling macs
  $remove_macs = "DELETE FROM macs WHERE client NOT IN ($client_list)";
  WebApp::execQuery($remove_macs);
}

/**
 * Check the registration status of the router.
 * If the router is not registered (or the registration has expired)
 * disable the router (which means that the client interface cannot be
 * used anymore).
 */
function check_status()
{
  //get eth0 MAC
  $mac = system(APP_PATH.'scripts/get-mac.sh');
  $mac = trim($mac);
  $mac = rawurlencode($mac);

  $url = ROUTER_ADMIN_URL.'check-router.php';
  $reply = file("$url?$mac");
  $status = trim($reply[0]);

  if ($status=='NOK')  system('rm '.APP_PATH.'.su/routerpasswd');
}
?>