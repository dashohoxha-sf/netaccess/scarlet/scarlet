#!/usr/bin/php -q
<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * This script should be called daily.
 * It gets the upload/download traffic of the last day (yesterday)
 * for each mac of each client and stores it in the table 'traffic_logs'.
 * It also updates the upload_limit and download_limit of the client
 * by subtracting the traffic of the last day from the available traffic
 * of the client.
 */

define("APP_PATH", dirname(dirname(__FILE__)).'/');
define("APP_URL", '../');
include_once APP_PATH.'webapp.php';

$rs = init();

$time1 = strtotime('2006-12-1');
$time2 = strtotime('2006-12-31');
$day = 24*60*60;

for ($t=$time1; $t <= $time2; $t+=$day)
{
  $date = date('Y-m-d', $t);

  //set a random traffic consumption for each mac in the recordset
  set_random_values($rs);

  //save in the table 'traffic_logs' each record of the recordset
  save_traffic_logs($rs, $date);
}

exit(0);

/*------------------------ functions -----------------------------*/

function init()
{
  WebApp::execQuery("DELETE FROM traffic_logs");

  srand((double)microtime()*1000000);

  //get a recordset of client/macs
  $rs = WebApp::execQuery("SELECT client, mac FROM macs ORDER BY client");

  //extend this recordset with upload/download columns
  $rs->addCol('upload', 0.0);
  $rs->addCol('download', 0.0);

  return $rs;
}

function set_random_values(&$rs)
{
  $rs->MoveFirst();
  while (!$rs->EOF())
    {
      $mac = $rs->Field('mac');
      $upload = rand(50, 100) / 3.0;
      $download = rand(500, 1000) / 3.0;
      $rs->setFld('upload', $upload);
      $rs->setFld('download', $download);
      $rs->MoveNext();
    }
}

/** Save in the table 'traffic_logs' each record of the recordset. */
function save_traffic_logs(&$rs, $date)
{
  $arr_values = array();
  $rs->MoveFirst();
  while (!$rs->EOF())
    {
      extract($rs->Fields());
      $upload = number_format($upload, 1);
      $download = number_format($download, 1);
      $arr_values[] = "('$client','$mac','$date','$upload','$download')";
      $rs->MoveNext();
    }

  $values = implode(', ', $arr_values);
  if ($values=='')  return;

  WebApp::execQuery("INSERT INTO traffic_logs VALUES $values");
}
?>
