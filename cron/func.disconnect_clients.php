<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * Disconnects all the macs of the clients in $client_list (which is
 * a comma separated list of clients): changes the status of the macs
 * to disconnected (in the table 'macs'), forbids the access of the macs
 * to internet, and also adds a log record for each mac that is disconnected.
 */
function disconnect_clients($client_list, $comment)
{
  if (trim($client_list)=='') return;

  //quote each client in $client_list
  $arr_clients = explode(',', $client_list);
  $client_list = "'" . implode("', '", $arr_clients) . "'";

  //get a list of the expired macs
  $get_macs = ("SELECT client, mac FROM macs "
               . "WHERE client IN ($client_list) AND connected='true'");
  $rs = WebApp::execQuery($get_macs);

  //disconnect them in 'macs'
  $disconnect_macs = ("UPDATE macs SET connected='false' "
                      . "WHERE client IN ($client_list)");
  WebApp::execQuery($disconnect_macs);

  //update 'server-config/allowed-macs'
  update_allowed_macs();

  //forbid in firewall each MAC that was disconnected 
  //and add a log record for it
  while (!$rs->EOF())
    {
      $client = $rs->Field('client');
      $mac = $rs->Field('mac');

      //deny access to internet
      shell(APP_PATH."server-config/firewall/mac-deny.sh $mac");

      //add a log record
      $d = "Source=program, Client=$client, MAC=$mac, Comment: $comment";
      log_event('-MAC', $d);
      $rs->MoveNext();

      //send a notification message
      mail('root', 'disconnected', "$client/$mac/$comment");
    }

  $rs->MoveFirst();
  if (!$rs->EOF())  //there are some macs that were disconnected
    {
      //reinit the clients of the script 'check-traffic-limits.php'
      shell('touch cron/reinit-clients.flag');
    }
}

/**
 * Get all the connected MACs from 'macs' and save them in the file
 * 'server-config/allowed-macs', which is used to build the firewall.
 */
function update_allowed_macs()
{
  $rs = WebApp::execQuery("SELECT mac FROM macs WHERE connected='true'");
  $arr_macs = $rs->getColumn('mac');
  $macs = implode("\n", $arr_macs);

  //write the macs to the file
  $fname = APP_PATH.'server-config/allowed-macs';
  $fp = fopen($fname, 'w');
  fputs($fp, $macs);
  fclose($fp);
}
?>