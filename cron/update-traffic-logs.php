#!/usr/bin/php -q
<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * This script should be called daily.
 * It gets the upload/download traffic of the last day (yesterday)
 * for each mac of each client and stores it in the table 'traffic_logs'.
 * It also updates the upload_limit and download_limit of the client
 * by subtracting the traffic of the last day from the available traffic
 * of the client.
 */

define("APP_PATH", dirname(dirname(__FILE__)).'/');
define("APP_URL", '../');
include_once APP_PATH.'webapp.php';

$stats_path = APP_PATH.'modules/admin/clients/details/stats/';
include_once $stats_path.'funcs.get_traffic.php';

//get a recordset of clients and their limits
$query = ("SELECT client, upload_limit, download_limit, last_update_time"
          . " FROM clients");
$clients = WebApp::execQuery($query);

//update the limits of each client
while (!$clients->EOF())
{
  $client = $clients->Fields();
  update_client($client);
  $clients->MoveNext();
}

exit(0);

/*------------------------ functions -----------------------------*/

/**
 * Update the upload_limit, download_limit and last_update_time
 * of the given client. 
 * The update is done in steps of at most one day, from the given
 * last_update_time up to the last midnight. The first step is from
 * last_update_time up to the midnight of that day, and the other steps
 * are from midnight to midnight. For each of the update steps (time
 * intervals) the traffic of the client during this interval is calculated,
 * and a log record is added in traffic_logs for each mac of the client.
 */
function update_client($arr_client)
{
  //get variables: $client, $upload_limit, $download_limit, $last_update_time
  extract($arr_client);

  //init the values of upload/download and log records 
  //from the last_update_time up to last midnight
  $upload = 0.0;
  $download = 0.0;
  $arr_log_recs = array();

  //get a list of the macs of the client
  $rs = WebApp::execQuery("SELECT mac FROM macs WHERE client='$client'");
  $arr_macs = $rs->getColumn('mac');

  //get the upload, download and log records of the client 
  //for all the past intervals from the last_update_time up to last midnight
  $hour = 60*60;
  $day = 24*$hour;
  $last_midnight = floor(time() / $day) * $day;
  $t1 = strtotime($last_update_time);
  $t1 = floor($t1/$hour)*$hour;
  $t2 = floor($t1/$day)*$day + $day;
  while ($t2 <= $last_midnight)
    {
      //get the upload/download values for the interval ($t1, $t2)
      //as well as the log records for each mac
      list($upld, $downld, $log_recs) 
        = get_client_traffic($t1, $t2, $client, $arr_macs);

      //sum up the traffic values and save the log records
      $upload += $upld;
      $download += $downld;
      if ($log_recs!='')  $arr_log_recs[] = $log_recs;

      //get the next interval
      $t1 = $t2;
      $t2 += $day;
    }

  //save (store in DB) the log records
  $log_records = implode(', ', $arr_log_recs);
  if ($log_records!='')
    WebApp::execQuery("INSERT INTO traffic_logs VALUES $log_records");

  //convert bytes to MB
  $upload = $upload / (1024.0*1024.0);
  $download = $download / (1024.0*1024.0);

  //update client limits
  $upload_limit -= $upload;
  $download_limit -= $download;
  $last_update_time = date('Y-m-d H:i:s', $last_midnight);
  $query = ("UPDATE clients"
            . " SET upload_limit = '$upload_limit',"
            . "     download_limit = '$download_limit',"
            . "     last_update_time = '$last_update_time'"
            . " WHERE client = '$client'");
  WebApp::execQuery($query);
}

/**
 * Calculate the upload/download traffic for the given period
 * for each mac in the array and sum them up. Keep also a log
 * record for each mac in the given interval. 
 * Return array(upload, download, log_records).
 */
function get_client_traffic($start, $end, $client, $arr_macs)
{
  $hour = 60*60;
  $day = 24*$hour;
  $resolution = ((time() - $end) > $day ? $day : $hour);

  $upload = 0.0;
  $download = 0.0;
  $arr_log_recs = array();

  for ($i=0; $i < sizeof($arr_macs); $i++)
    {
      $mac = $arr_macs[$i];      

      //get upload/download for this mac in the given interval
      list($upld, $downld) = get_mac_traffic($mac, $start, $end, $resolution);

      //sum up the traffic values
      $upload += $upld;
      $download += $downld;

      //keep a traffic log record for this mac and this time
      $time = date('Y-m-d H:i:s', $start);
      $upld = $upld / (1024.0*1024.0);  //convert bytes to MB
      $downld = $downld / (1024.0*1024.0);      
      $arr_log_recs[] = "('$client','$mac','$time','$upld','$downld')";
    }

  //convert to a string the array of the log records
  $log_records = implode(', ', $arr_log_recs);

  return array($upload, $download, $log_records);
}
?>