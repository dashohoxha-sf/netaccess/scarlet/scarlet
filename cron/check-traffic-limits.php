#!/usr/bin/php -q
<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * This script is usually restarted from a cron job that runs each night.
 *
 * It calculates the traffic of each client from the time it startes
 * up to now and checks that it has not passed the limits of the client.
 * If the limits are passed, then the connection of all the macs 
 * of the client is interrupted.
 *
 * The script stays in memory and makes a check each 5 minutes.
 *
 * If the file 'reinit-clients.flag' exists, then the list of the clients
 * to be checked (and their macs) is re-initialized. This flag-file 
 * is created when a client is added/deleted, or when a MAC is 
 * connected/disconnected
 */

define("APP_PATH", dirname(dirname(__FILE__)).'/');
define("APP_URL", '../');
include_once APP_PATH.'webapp.php';

$stats_path = APP_PATH.'modules/admin/clients/details/stats/';
include_once $stats_path.'funcs.get_traffic.php';
include_once dirname(__FILE__).'/func.disconnect_clients.php';

//create the recordset $rs_clients (as a global var)
init_clients();

//get the upload/download traffic of each client 
//from the last time it was updated in the database up to now
update_client_traffic('init');

//keep the script running all the time, in order to make it more efficient
while (true)
{
  //clients are reinitialized only if the file 'reinit-clients.flag' exists
  $fname = dirname(__FILE__).'/reinit-clients.flag';
  if (file_exists($fname))  reinit_clients();

  //get the upload/download traffic of each client for the last minutes
  //and sum it up with the rest of traffic
  update_client_traffic();

  //check the traffic of each client that it has not passed the limits
  check_client_traffic();

  //wait 5 minutes
  //sleep(300);
  sleep(60);
}

/*------------------------ functions -----------------------------*/

/**
 * Clients are reinitialized only if the file 'reinit-clients.flag'
 * exists. This flag-file is created when a client is added/deleted,
 * or when a MAC is connected/disconnected (in order to update the data
 * of the clients and macs).
 */
function reinit_clients()
{
  $fname = dirname(__FILE__).'/reinit-clients.flag';
  if (!file_exists($fname))  return;
    
  save_client_traffic();
  init_clients();
  load_client_traffic();
  shell("rm $fname");
}

/**
 * Create the recordset $rs_clients (as a global var) which has these
 * fields for each client: client, upload_limit, download_limit,
 * last_update_time, macs (a space separated list of the macs of the client), 
 * upload, download (counted from the last_update_time up to now).
 */
function init_clients()
{
  global $rs_clients, $last_update_time;

  //get a list of clients that have any macs connected and their traffic limits
  $get_clients = ("SELECT DISTINCT client, "
                  . " upload_limit, download_limit, last_update_time"
                  . " FROM clients LEFT OUTER JOIN macs USING(client)"
                  . " WHERE macs.connected='true'"
                  . " ORDER BY client");
  $rs_clients = WebApp::execQuery($get_clients);
  $rs_clients->addCol('macs', '');
  $rs_clients->addCol('upload', 0.0);
  $rs_clients->addCol('download', 0.0);

  //get the macs of each client as a space separated list
  //and store them in $rs_clients
  get_client_macs();

  $last_update_time = floor(time() / 10) * 10;
}

/**
 * Get the macs of each client as a space separated list
 * and store them in $rs_clients.
 */
function get_client_macs()
{
  global $rs_clients;

  $query = ("SELECT client, mac FROM macs "
            . " WHERE connected='true' "
            . " ORDER BY client");
  $rs_macs = WebApp::execQuery($query);

  $rs_clients->MoveFirst();
  if ($rs_clients->EOF())  return;
  $client = $rs_clients->Field('client');

  $rs_macs->MoveFirst();
  $macs = $rs_macs->Field('mac');

  $rs_macs->MoveNext();
  while (!$rs_macs->EOF() and !$rs_clients->EOF())
    {
      if ($client==$rs_macs->Field('client'))
        {
          $macs .= ' ' . $rs_macs->Field('mac');
        }
      else
        {
          $rs_clients->setFld('macs', $macs);
          while ($client!=$rs_macs->Field('client') and !$rs_clients->EOF())
            {
              $rs_clients->MoveNext();
              $client = $rs_clients->Field('client');
            }
          $macs = $rs_macs->Field('mac');      
        }

      $rs_macs->MoveNext();
    }

  if (!$rs_clients->EOF()) $rs_clients->setFld('macs', $macs);
}

/**
 * For each client get the traffic from the last update time up to now
 * and add it to the traffic of the client in $rs_clients.
 * If the parameter $init is 'true' (which happens only when the script
 * is started), last update time is the one that is stored in the database
 * for each client.
 */
function update_client_traffic($init =UNDEFINED)
{
  global $rs_clients, $last_update_time;

  $now = floor(time() / 10) * 10;

  $rs_clients->MoveFirst();
  while (!$rs_clients->EOF())
    {
      $upload = $rs_clients->Field('upload');
      $download = $rs_clients->Field('download');
      if ($init=='init')
        {
          $last_db_update = $rs_clients->Field('last_update_time');
          $last_db_update = strtotime($last_db_update);
        }
      
      $macs = $rs_clients->Field('macs');
      $arr_macs = explode(' ', trim($macs));
      for ($i=0; $i < sizeof($arr_macs); $i++)
        {
          $mac = $arr_macs[$i];

          if ($init=='init')
            list($up,$down) = get_current_traffic($mac,$last_db_update,$now);
          else
            list($up,$down) = get_mac_traffic($mac,$last_update_time,$now,10);
          $upload += $up;
          $download += $down;
        }

      $rs_clients->setFld('upload', $upload);
      $rs_clients->setFld('download', $download);

      $rs_clients->MoveNext();
    }

  $last_update_time = $now;
}

/**
 * For each client check that the client traffic has not passed the limits.
 * If yes, then interrupt the connection for all the macs of the client.
 */
function check_client_traffic()
{
  global $rs_clients, $last_update_time;

  //get a list of the clients who have passed the traffic limits
  $arr_clients = array();
  $rs_clients->MoveFirst();
  while (!$rs_clients->EOF())
    {
      $upload = $rs_clients->Field('upload');
      $download = $rs_clients->Field('download');
      $upload_limit = $rs_clients->Field('upload_limit');
      $download_limit = $rs_clients->Field('download_limit');

      $upload_limit *= 1024.0 * 1024.0;
      $download_limit *= 1024.0 * 1024.0;
      if (($upload >= $upload_limit) or ($download >= $download_limit))
        {
          $arr_clients[] = $client;

          //update the traffic limits of the client
          $upld_limit = ($upload_limit - $upload) / (1024.0 * 1024.0);
          $downld_limit = ($download_limit - $download) / (1024.0 * 1024.0);
          $update_time = date('Y-m-d H:i:s', $last_update_time);
          $query = ("UPDATE clients"
                    . " SET upload_limit = '$upld_limit',"
                    . "     download_limit = '$downld_limit',"
                    . "     last_update_time = '$update_time'"
                    . " WHERE client = '$client'");
          WebApp::execQuery($query);
        }

      $rs_clients->MoveNext();
    }
  $client_list = implode(',', $arr_clients);

  //disconnect these clients
  disconnect_clients($client_list, 'traffic limit passed');
}

/**
 * Write to the given file the last_update_time and the
 * upload/download values for each client at that time.
 */
function save_client_traffic()
{
  global $rs_clients, $last_update_time;

  $time = date('Y-m-d H:i:s', $last_update_time);
  $client_traffic = "last_update_time $last_update_time $time\n";

  $rs_clients->MoveFirst();
  while (!$rs_clients->EOF())
    {
      $client = $rs_clients->Field('client');
      $upload = $rs_clients->Field('upload');
      $download = $rs_clients->Field('download');
      $client_traffic .= "$client $upload $download\n";

      $rs_clients->MoveNext();
    }

  $fname = dirname(__FILE__).'/client-traffic.txt';
  write_file($fname, $client_traffic);
}

/**
 * Read from file 'client-traffic.txt' the last_update_time and the
 * upload/download values for each client at that time.
 */
function load_client_traffic()
{
  global $rs_clients, $last_update_time;

  //read the file 'client-traffic.txt' into an array of lines
  $fname = dirname(__FILE__).'/client-traffic.txt';
  $arr_lines = file($fname);

  //get the last update time from the first line
  $arr = explode(' ', $arr_lines[0]);
  $last_update_time = $arr[1];

  //get the traffic values of the clients from the rest of the lines
  for ($i=1; $i < sizeof($arr_lines); $i++)
    {
      $line = trim($arr_lines[$i]);
      if ($line=='')  continue;
      list($client, $upload, $download) = explode(' ', $line);
      $rs_clients->find("client='$client'");
      if (!$rs_clients->EOF())
        {
          $rs_clients->setFld('upload', $upload);
          $rs_clients->setFld('download', $download);
        }
    }
}
?>