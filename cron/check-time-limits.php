#!/usr/bin/php -q
<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * This script checks the expiration time of the clients, and if it
 * has passed, changes the status of their MACs to disconnected 
 * (in the table macs in DB).
 * Then it refreshes the list of the allowed macs in the firewall
 * and recreates the firewall. 
 * This script should be called periodically by a cron job once
 * an hour or so.
 */

/*
if ($argc < 2)
{
  print "Usage: $argv[0] output_dir file.xml \n";
  exit(1);
}
$dir = $argv[1];
$fname = $argv[2];
*/

define("APP_PATH", dirname(dirname(__FILE__)).'/');
define("APP_URL", '../');
include_once APP_PATH.'webapp.php';

include_once dirname(__FILE__).'/func.disconnect_clients.php';

disconnect_expired_clients();

exit(0);

/*------------------------ functions -----------------------------*/

/**
 * Disconnect the MACs of all the clients whose expiration time has passed.
 */
function disconnect_expired_clients()
{
  //get the expired clients
  $clients = "SELECT client FROM clients WHERE expiration_time < NOW()";
  $rs = WebApp::execQuery($clients);

  //if empty, return
  if ($rs->EOF())  return false;

  //convert them to a list
  $arr_clients = $rs->getColumn('client');
  $client_list = implode(',', $arr_clients);

  //disconnect them
  disconnect_clients($client_list, 'time limit expiration');
}
?>