#!/bin/bash

cd $(dirname $0)

rm -rf var/
mkdir -p var/www/htdocs/

cp -a ../scarlet var/www/htdocs/
makepkg scarlet-2.1.tgz

scp scarlet-2.1.tgz dasho@10.0.0.11:

