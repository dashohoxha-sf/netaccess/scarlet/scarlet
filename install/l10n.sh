#!/bin/bash
### compile the translation files etc.

### go to this dir
cd $(dirname $0)

### get config parameters
. ../sysconfig

### modify the language options of the application
sed -i ../config/const.Options.php \
    -e "/^define('LNG'/ c define('LNG', '$LNG');"
sed -i ../config/const.Options.php \
    -e "/^define('CODESET'/ c define('CODESET', '$CODESET');"

### compile translation files to binary format
langs="en sq_AL nl it de"
for lng in $langs
do
  ../l10n/msgfmt.sh $lng
done

