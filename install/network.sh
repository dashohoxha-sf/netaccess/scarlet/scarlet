#!/bin/bash
### setup network config files

### go to this dir
cd $(dirname $0)

### get config parameters
. ../sysconfig

### modify the ACCESS_IP
sed -i ../config/const.Options.php \
    -e "/^define('ACCESS_IP'/ c define('ACCESS_IP', '$ACCESS_IP');"

### create the file 'gateway.cfg'
cp ../server-config/templates/gateway.cfg ../server-config/gateway.cfg

### set hostname
echo "gateway.localnet.net" > /etc/HOSTNAME

