#!/bin/bash
### create/modify some system scripts or config files

### go to this dir
cd $(dirname $0)

### include the configuration file
. ../sysconfig

### change the port of ssh service
sed -i /etc/ssh/sshd_config -e "/^#Port/ c Port 2222"
/etc/rc.d/rc.sshd restart

### modify the /etc/rc.d/rc.local file
APP_PATH=$(dirname $(pwd))
sed -e "s#|APP_PATH|#$APP_PATH#g"  \
    ../server-config/templates/rc.local > /etc/rc.d/rc.local

### enable serial console login
sed -i /etc/inittab -e "s/^#s1/s1/"          # uncomment the serial line
sed -i /etc/securetty -e "s/^#ttyS0/ttyS0/"  # uncomment ttyS0

### set the admin email
sed -i /etc/mail/aliases -e "/### admin email/,+1 d"
cat <<EOF >> /etc/mail/aliases
### admin email
root:		$ADMIN_EMAIL
EOF
newaliases
