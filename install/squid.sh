#!/bin/bash
### squid installation

### go to this directory
cd $(dirname $0)

### download the squid package from http://www.merlinobbs.net/squid/
if [ ! -f squid-2.6.STABLE1-i386-1.tgz.asc ]
then
  wget http://www.merlinobbs.net/squid/squid-2.6.STABLE1-i386-1.tgz
  wget http://www.merlinobbs.net/squid/squid-2.6.STABLE1-i386-1.tgz.asc
fi

### check the integrity of the package
if [ ! -f MyKey.asc ]
then
  wget http://www.merlinobbs.net/MyKey.asc
  gpg --import MyKey.asc
  gpg squid-2.6.STABLE1-i386-1.tgz.asc
fi

### install package
if [ "$(whereis squid)" = "squid:" ]
then
  installpkg squid-2.6.STABLE1-i386-1.tgz
fi

### create a shell script that can start/stop/restart squid proxy server
cat <<EndOfScript > /etc/rc.d/rc.squid
#!/bin/sh
# nome del file: /etc/rc.d/rc.squid
echo -n ' squid '
case "\$1" in
start)
/usr/sbin/squid -D
;;
stop)
/usr/sbin/squid -k shutdown
;;
restart)
/usr/sbin/squid -k reconfigure
;;
*)
echo "Usage: \$0 {start|stop|restart}"
;;
esac
#
# EOF (c) 2005 Stefano Tagliaferri - GPL license
EndOfScript

### make it executable
chmod +x /etc/rc.d/rc.squid

### start squid at bootup, append these lines at /etc/rc.d/rc.inet2
sed -i /etc/rc.d/rc.inet2 -e "/^# Start SQUID/,+4 d"
cat <<EndOfPatch >> /etc/rc.d/rc.inet2
# Start SQUID (Squid proxy server):
if [ -x /etc/rc.d/rc.squid ]; then
  . /etc/rc.d/rc.squid start
fi

EndOfPatch


### create squid config files
if [ ! -f /etc/squid/squid.conf ]
then
  cp /etc/squid/mime.conf.default /etc/squid/mime.conf
  cp /etc/squid/msntauth.conf.default /etc/squid/msntauth.conf
  cp /etc/squid/squid.conf.default /etc/squid/squid.conf
fi
### create swap directories
mkdir -p /var/log/squid
mkdir -p /var/log/squid/logs
mkdir -p /var/log/squid/cache
chown nobody:nobody -R /var/log/squid/
squid -z

### start squid
/etc/rc.d/rc.squid stop 2> /dev/null
/etc/rc.d/rc.squid start

### rotate the squid logs
cp ../server-config/templates/squid.logrotate /etc/logrotate.d/squid
 
