#!/bin/bash
### modify the php configuration

### make sure that some php directives have proper settings
sed -i /etc/apache/php.ini \
    -e "/^error_reporting/ c error_reporting = E_ALL & ~E_NOTICE"
sed -i /etc/apache/php.ini \
    -e "/^display_errors/ c display_errors = On"

### add index.php to the list of files that will be served 
### as directory indexes (by apache)
sed -i /etc/apache/mod_php.conf -e /DirectoryIndex/d
echo "DirectoryIndex index.php" >> /etc/apache/mod_php.conf

