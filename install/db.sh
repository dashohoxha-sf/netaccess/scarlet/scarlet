#!/bin/bash
### install and init the database

### go to this directory
cd $(dirname $0)

### use the small config file for mysqld
cp /etc/my-small.cnf /etc/my.cnf

### init mysql
echo "----- Initializing mysql..."
su - mysql mysql_install_db
echo "----- Starting mysql..."
chmod +x /etc/rc.d/rc.mysqld
su - mysql /etc/rc.d/rc.mysqld restart
sleep 3
# echo "----- Setting the password of root for mysql..."
# echo -n "Enter the root password: "
# read passwd
. ../sysconfig
passwd=$DB_PASSWD
su - mysql mysqladmin -u root password "$passwd"

### changing the mysql password at the config files of the application
echo "----- Changing the mysql password at the config files of the application..."
sed -e "/passwd=/ c passwd=$passwd" -i ../db/vars
sed -e "/DBPASS/ c define('DBPASS', '$passwd');" -i ../config/const.DB.php
sleep 2

### initialise the database
echo "----- Initialising the database..."
../db/init.sh

