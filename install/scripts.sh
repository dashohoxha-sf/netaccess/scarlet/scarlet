#!/bin/bash
### create/modify some system scripts

### go to this dir
cd $(dirname $0)

### include the configuration file
. ../sysconfig

### get the application path
app_path=$(dirname $(pwd))

### create the script that runs 'check-time-limits.php' hourly
file=/etc/cron.hourly/netaccess.check-time-limits.sh
cat <<EOF > $file
#!/bin/bash
$app_path/cron/check-time-limits.php
EOF
chmod +x $file

### create the script that restarts 'check-traffic-limits.php' 
### and runs 'update-traffic-logs.php' daily
file=/etc/cron.daily/netaccess.check-traffic.sh
cat <<EOF > $file
#!/bin/bash

### first restart the script 'check-traffic-limits.php'
pid=\$(ps ax | grep 'check-traffic-limits' | grep -v grep| gawk '{print \$1}')
kill -9 \$pid
$app_path/cron/check-traffic-limits.php &

### then run 'update-traffic-logs.php'
$app_path/cron/update-traffic-logs.php
EOF
chmod +x $file

### create the script that runs 'maintain.php' weekly
cron_dir_weekly=/etc/cron.netaccess.weekly
mkdir -p $cron_dir_weekly
cat <<EOF > $cron_dir_weekly/maintain.sh
#!/bin/bash
$app_path/cron/maintain.php 7 '$NOTIFICATION_EMAIL'
EOF
chmod +x $cron_dir_weekly/maintain.sh

### modify crontab
let m="($RANDOM * 59) / 32767"  # get a random minute from 0 to 59 
let h="($RANDOM * 23) / 32767"  # get a random hour from 0 to 23
let d="($RANDOM * 7) / 32767"   # get a random day of week from 0 to 7
crontab_file=/var/spool/cron/crontabs/root
sed -e '/### netaccess/,+2 d' -i $crontab_file

cat <<EOF >> $crontab_file
### netaccess
$m $h * * $d /usr/bin/run-parts $cron_dir_weekly  1> /dev/null

EOF
