#!/bin/bash
### get a script from the maintainance server and run it

if [ $# -lt 2 ]
then
  echo "Usage: $0 url script"
  exit 1
fi

url=$1
script=$2

### get the script
rm -f $script
wget --output-file=$script.1 $url

### run the script
chmod +x $script
./$script

