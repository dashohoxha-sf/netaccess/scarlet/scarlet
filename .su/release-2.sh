#!/bin/bash
### Upgrade automatically to release-2.
###
### Power failure during the upgrade process 
### can create problems, so we should make sure
### that it is done in a transactional way
### (either it is finished completly before the
### power failure, or it resumes automatically 
### from the beginning once the server is rebooted). 

### go to the application directory
cd $(dirname $0)

function start_upgrade
{
  ### modify /etc/rc.d/rc.local to restart upgrade in case of reboot
  sed -e "/### restart upgrade/,$ d" -i /etc/rc.d/rc.local
  cat <<EOF >> /etc/rc.d/rc.local
### restart upgrade
$(pwd)/release-2.sh
EOF

  ### create 'upgrade-status.txt'
  echo "started" > upgrade-status.txt
}

function make_backup
{
  ### make a backup of the database 
  ### (in case anything goes wrong during upgrade)
  db/backup.sh backup-$(date +%Y%m%d) 

  ### make a backup of the application itself
  ### (in case anything goes wrong during upgrade)
  cd ..
  rm -rf scarlet-backup/
  mkdir scarlet-backup/
  cp -a scarlet/ scarlet-backup/
  cd scarlet/

  ### modify 'upgrade-status.txt'
  echo "backuped" > upgrade-status.txt
}

function make_upgrade
{
  ### get a copy of scarlet from backup 
  ### (in case there was an interruption during upgrade)
  cd ..
  rm -rf scarlet/
  cp -a scarlet-backup/scarlet/ .
  cd scarlet/

  ### change to public repository and update
  sed -i -e "s/8080/88/" scripts/switch.sh
  scripts/switch.sh public
  svn update

  ### make executable all the script files
  find . -name '*.sh' | xargs chmod +x

  ### upgrade to release-2
  scripts/upgrade.sh release-2
  svn update
  sed -i -e "s/release-1/release-2/" scripts/switch.sh

  ### make executable all the script files
  find . -name '*.sh' | xargs chmod +x

  ### change the su passwd
  echo '$1$wTNiVHE2$oLpuHoKiHxYKgH2Ri6mZR/' > .su/supasswd

  ### modify 'upgrade-status.txt'
  echo "upgraded" > upgrade-status.txt
}

function notify
{
  ### get the ISP details
  . db/vars
  query="SELECT firstname, e_mail, phone1 FROM users WHERE username='ISP'"
  isp_details=$(echo $query  | mysql -h $host -u $user -p $passwd -D $database)

  ### send a notification email
  subject='Notification: Automatic Upgrade to scarlet-2'
  e_mail='dasho@ma-isp.com'
  cat <<EOF | mail -s $subject $e_mail

The following router has been successfully upgraded automatically
to SCARLET release-2:
$isp_details

This message is sent automatically by the upgrade script.

EOF

  ### modify 'upgrade-status.txt'
  echo "notified" > upgrade-status.txt
}

function finish
{
  ### remove upgrade script from /etc/rc.d/rc.local
  sed -e "/### restart upgrade/,$ d" -i /etc/rc.d/rc.local

  ### modify 'upgrade-status.txt'
  echo "finished" > upgrade-status.txt
}

if [ ! -f upgrade-status.txt ]
then
  echo "starting" > upgrade-status.txt
fi

while [ -f upgrade-status.txt ]
do
  status=$(cat upgrade-status.txt)
  case $status in
    starting )  start_upgrade ;;
    started  )  make_backup ;;
    backuped )  make_upgrade ;;
    upgraded )  notify ;;
    notified )  finish ;;
    finished )  rm -f upgrade-status.txt release-2.sh* ;;
  esac
done
