#!/bin/bash
### import for the first time the document in docbookwiki
### takes as parameter the path of the docbookwiki application

if [ "$1" = "" ]
then
  echo "Usage: $0 docbookwiki_path"
  exit 1
fi
docs_path=$1

### details of the xml document
xml_doc=$(dirname $0)/slackrouter_en.xml
book_id=slackrouter
book_title="Slack Router"
lng=en
fname_xml=$(basename $xml_doc)

### import the xml document in the docbookwiki
cp $xml_doc $docs_path/content/initial_xml/uploaded/
cd $docs_path/content/
./import.sh initial_xml/uploaded/$fname_xml
cd ..

### add it to the menu and update the menu items 
cd templates/menu/
item='<item id="'$book_id'" caption="'$book_title'" bookid="'$book_id'"/>'
sed -e "/tools_and_apps/ a $item" -i menu.xml
sed -e "/$book_id/ s/^/    /" -i menu.xml

xsl_stylesheet=../admin/edit_menu/menu/xsl/menu_items.xsl
xsltproc $xsl_stylesheet menu.xml > menu_items.js
cd ../..

### add the doc in maintain list
cd content/SVN/

. svn_dir.txt
xml_file=$svn_dir/${book_id}_${lng}.xml

../implode/implode.sh $book_id $lng
cp ../implode/tmp/$book_id.xml $xml_file

svn add $xml_file
svn commit $xml_file -m "$(hostname -i):$(pwd)"

sed -e "/tools_and_apps/ a '$book_id $lng'" -i book_list.sh
sed -e "/$book_id/ s/^/    /" -i book_list.sh

cd ../..

