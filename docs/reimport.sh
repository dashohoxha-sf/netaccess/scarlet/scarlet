#!/bin/bash
### import a new version of the document in docbookwiki
### takes as parameter the path of the docbookwiki application

if [ "$1" = "" ]
then
  echo "Usage: $0 docbookwiki_path"
  exit 1
fi
docs_path=$1

### details of the xml document
xml_doc=$(dirname $0)/slackrouter_en.xml
book_id=slackrouter
book_title="Slack Router"
lng=en
fname_xml=$(basename $xml_doc)

### import the xml document in the docbookwiki
cp $xml_doc $docs_path/content/initial_xml/uploaded/
cd $docs_path/content/
./import.sh initial_xml/uploaded/$fname_xml
cd ..

### update the SVN copy
content/SVN/commit.sh $book_id $lng

