#!/bin/bash
### get the document in the xml format and
### in other formats from docbookwiki

### go to this dir
cd $(dirname $0)

### book details
docs_path=/home/dasho/public_html/docs
book_id=slackrouter
lng=en

### copy the formats
rm -rf tar_gz
mkdir tar_gz
cp $docs_path/content/downloads/tar_gz/$book_id/$lng/* tar_gz/

### open the tar.gz files, and create a new archive
rm -rf slackrouter/
mkdir slackrouter
cd slackrouter/
for file in $(ls ../tar_gz/*.tar.gz) ; do tar xfz $file ; done
mv slackrouter_en_dsssl/slackrouter_en_dsssl.html .
mv slackrouter_en_xsl/slackrouter_en_xsl.html .
rmdir slackrouter_en_dsssl/media/
rmdir slackrouter_en_xsl/media/
cd ..
tar cfz slackrouter.tar.gz slackrouter

cp slackrouter/slackrouter_en.xml .
cp slackrouter/slackrouter_en_xsl.html .
cp slackrouter/slackrouter_en_xsl.txt .
cp slackrouter/slackrouter_en_xsl.pdf .

