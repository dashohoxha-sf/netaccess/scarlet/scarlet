#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define SRCUID 0    /*our UID so we can do things ourselves*/
#define NOBID  99   /*another possible ID (apache)*/
#define TGTUID 0 /*the desired run UID*/
#define TGTGID 0 /*the desired run group*/
#define TGTUSER "root"
#define TGTHOME "/root/" TGTUSER
int main(int argc, char**argv)
{
  size_t  i, n=0;
  char *buf;
  n = getuid();
  if(n!=NOBID && n!=SRCUID) exit(-1);
  for(i=1; i<argc; i++)  n += strlen(argv[i]);
  if(!n) exit(0);
  buf = malloc(n+argc+1);
  *buf = 0;
  for(i=1; i<argc; i++)
  {
    if(i>1) strcat(buf," ");
    strcat(buf,argv[i]);
  }
  setuid(TGTUID);
  setgid(TGTGID); /*users*/
  setenv("USER",TGTUSER,1);
  setenv("HOME",TGTHOME,1);
  /* printf("%s\n", buf); */
  system(buf);
}

