<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once 'global.php';

/**
 * Check that this router is a valid (registered/enabled) router.
 */
/*
function valid_router()
{
  //get eth0 MAC and the stored crypted mac
  $eth0_mac = shell('scripts/get-mac.sh');
  $eth0_mac = trim($eth0_mac);
  $crypted_mac = shell('cat .su/routerpasswd');
  $crypted_mac = trim($crypted_mac);

  $valid = ($crypted_mac == crypt($eth0_mac, $crypted_mac));
  return $valid;
}
*/

/** 
 * Checks if the given username and password are valid.
 * Returns true or false.
 */
function valid_user()
{
  $username = $_SERVER["PHP_AUTH_USER"];
  $password = $_SERVER["PHP_AUTH_PW"];

  if ($username=='superuser')
    {
      $passwd = shell('cat .su/supasswd');
      $passwd = trim($passwd);
      $rs = new EditableRS('user_data');
      $rs->addRec(array('password'=>$passwd));
    }
  else
    {
      $query = "SELECT * FROM users WHERE username='$username'";
      $rs = new EditableRS('user_data', $query);
      $rs->Open();
    }
  global $webPage;
  $webPage->addRecordset($rs);

  if ($rs->EOF())  return false; //query returned no records

  $crypted_passwd = $rs->Field('password');
  $valid = ($crypted_passwd == crypt($password, $crypted_passwd));

  return $valid;
}

function authenticate()
{
  header("WWW-Authenticate: Basic realm=\"SCARLET Admin\"");
  header("HTTP/1.0 401 Unauthorized");
  $host = $_SERVER['HTTP_HOST'];
  $file = $_SERVER['SCRIPT_NAME'];
  $url = 'http://'.$host.dirname($file);
  print "
<html>
<head>
  <title>Unauthorized</title>
  <meta http-equiv='refresh' content='2;url=$url'>
</head>
<body>
<h1>Sorry, you cannot access this page.</h1>
</body>
";

  exit;
}

/** add some state variables */
function init_user()
{
  $username = $_SERVER["PHP_AUTH_USER"];
  $rs = WebApp::openRS('user_data');

  WebApp::addSVar('username', $username, 'DB');
  WebApp::addSVar('password', $rs->Field('password'), 'DB');

  if ($username=='superuser')
    {
      define('SU', 'true');
      WebApp::addSVar('u_id', '0', 'DB');
    }
  else
    {
      //save some data in the session 
      WebApp::addSVar('u_id', $rs->Field('user_id'), 'DB');
      WebApp::addSVar('username', $username, 'DB');
      WebApp::addSVar('access_rights', $rs->Field('access_rights'), 'DB');
      WebApp::addSVar('firstname', $rs->Field('firstname'));
      WebApp::addSVar('lastname', $rs->Field('lastname'));

      //add a log record
      log_event('login', "Source=$username, Name=$firstname $lastname");
    }
}

//authenticate if the user is unknown or not valid
if (!isset($_SERVER['PHP_AUTH_USER']))  authenticate();
else if (!valid_user())  authenticate();

//the user is authenticated successfully
//add some state variables about him (like u_id, access_rights, etc.)
init_user();

//if the router is not valid, do not open the admin interface
/*
if (!valid_router())
{
  define('UNREGISTERED', 'true');
  $msg = T_("This is an unregistered copy of the SCARLET Connection Manager.");
  WebApp::message($msg);
}
*/
?>
