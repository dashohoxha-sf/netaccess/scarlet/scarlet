-- MySQL dump 10.9
--
-- Host: localhost    Database: netaccess
-- ------------------------------------------------------
-- Server version	4.1.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `netaccess`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `netaccess` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `netaccess`;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
CREATE TABLE `clients` (
  `client` varchar(20) NOT NULL default '',
  `firstname` varchar(100) default '',
  `lastname` varchar(100) default NULL,
  `e_mail` varchar(100) default NULL,
  `phone1` varchar(100) default NULL,
  `phone2` varchar(100) default NULL,
  `address` varchar(100) default NULL,
  `notes` text,
  `nr_connections` int(11) NOT NULL default '0',
  `expiration_time` datetime default '0000-00-00 00:00:00',
  `upload_limit` float NOT NULL default '0',
  `download_limit` float NOT NULL default '0',
  `last_update_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `password` varchar(100) default '',
  PRIMARY KEY  (`client`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages` (
  `id` varchar(10) NOT NULL default '',
  `name` varchar(100) NOT NULL default '',
  `charset` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
CREATE TABLE `logs` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `time` datetime NOT NULL default '0000-00-00 00:00:00',
  `event` varchar(100) NOT NULL default '',
  `details` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `macs`
--

DROP TABLE IF EXISTS `macs`;
CREATE TABLE `macs` (
  `client` varchar(30) NOT NULL default '',
  `mac` varchar(100) NOT NULL default '',
  `connected` varchar(5) NOT NULL default 'false',
  `timestamp` varchar(100) default '',
  `hostname` varchar(100) default '',
  PRIMARY KEY  (`mac`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
CREATE TABLE `session` (
  `id` varchar(255) NOT NULL default '',
  `vars` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `section` varchar(50) NOT NULL default '',
  `name` varchar(100) NOT NULL default '',
  `value` varchar(100) default '',
  PRIMARY KEY  (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `traffic_logs`
--

DROP TABLE IF EXISTS `traffic_logs`;
CREATE TABLE `traffic_logs` (
  `client` varchar(100) NOT NULL default '',
  `mac` varchar(100) NOT NULL default '',
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `upload` float unsigned default '0',
  `download` float unsigned default '0',
  PRIMARY KEY  (`mac`,`date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL auto_increment,
  `username` varchar(20) default NULL,
  `password` varchar(20) NOT NULL default '',
  `firstname` varchar(100) NOT NULL default '',
  `lastname` varchar(100) default NULL,
  `address` varchar(100) default NULL,
  `phone1` varchar(100) default NULL,
  `phone2` varchar(100) default NULL,
  `e_mail` varchar(100) default NULL,
  `notes` text,
  `access_rights` text,
  PRIMARY KEY  (`user_id`),
  UNIQUE KEY `u` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

